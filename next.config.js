/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/login", // Ganti dengan halaman yang ingin Anda jadikan halaman pertama
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
