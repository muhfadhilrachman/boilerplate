import { useState, useMemo } from "react";
import {
  useQuery,
  QueryFunction,
  UseQueryResult,
  QueryKey,
} from "@tanstack/react-query";

interface AsyncHookResult<T> {
  execute: (arg: any) => Promise<void>;
  status: "idle" | "fetching" | "resolved" | "rejected";
  value: T | null;
  error: any;
  errorMessage: string | null;
  fieldErrors: { [key: string]: string } | null;
}

export const useAsync = <T>(
  asyncFunction: (arg: any) => Promise<T>
): AsyncHookResult<T> => {
  const [status, setStatus] = useState<
    "idle" | "fetching" | "resolved" | "rejected"
  >("idle");
  const [value, setValue] = useState<T | null>(null);
  const [error, setError] = useState<any>(null);
  const errorMessage = useMemo(() => {
    if (error) {
      if (error.response && error.response.data) {
        return error.response.data.message;
      }
      return "Something error";
    }
    return null;
  }, [error]);

  const fieldErrors = useMemo(() => {
    if (error) {
      if (error.response && error.response.data) {
        return error.response.data.errors;
      }
      return {};
    }
    return null;
  }, [error]);

  const execute = async (arg: any): Promise<void> => {
    setStatus("fetching");
    setValue(null);
    setError(null);
    try {
      const { data }: any = await asyncFunction(arg);
      setValue(data);
      setStatus("resolved");
    } catch (err) {
      setError(err);
      setStatus("rejected");
      throw err;
    }
  };

  return { execute, status, value, error, errorMessage, fieldErrors };
};

interface GetQueryHookResult<T> {
  data: T | undefined;
  status: UseQueryResult<T, any>["status"];
  // isIdle: boolean;
  errorMessage: string | null;
  refetch: UseQueryResult<T, any>["refetch"];
  isFetching: UseQueryResult<T, any>["isFetching"];
}

interface GetQueryOptions<T> {
  queryKey: string;
  queryFn: QueryFunction<T, QueryKey>;
  enabled?: boolean;
}

export const useGetQuery = <T>({
  queryKey,
  queryFn,
  enabled = true,
}: GetQueryOptions<T>): GetQueryHookResult<T> => {
  const { data, status, error, refetch, isFetching } = useQuery<T, any>(
    [queryKey],
    queryFn,
    {
      enabled,
    }
  );

  // const isIdle = status === 'idle'; // Determine isIdle based on the status value

  const errorMessage = useMemo(() => {
    if (error) {
      if (error.response && error.response.data) {
        return error.response.data.message;
      }
      return "Something error";
    }
    return null;
  }, [error]);

  return {
    data,
    status,
    errorMessage,
    refetch,
    isFetching,
    // isIdle  // Include isIdle in the returned object
  };
};
