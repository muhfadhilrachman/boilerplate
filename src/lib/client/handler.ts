import * as storage from "../storage";

const errorPrefix = "[ERROR] ";

const errorHandler = {
  somethingWrong(): void {
    console.error(errorPrefix, "Something wrong");
  },

  badRequest(): void {
    console.error(errorPrefix, "Bad Request");
  },

  notAuthorize(): void {
    storage.clearStorage();
    window.location.href = "/login";
    console.error(errorPrefix, "Not Authorize");
  },
};

const errorMiddleware = (error: any): Promise<never> => {
  const { response } = error;

  if (
    typeof response === "undefined" ||
    typeof response.status === "undefined"
  ) {
    errorHandler.somethingWrong();
    return Promise.reject(error);
  }

  switch (response.status) {
    case 400:
      errorHandler.badRequest();
      break;
    case 401:
      errorHandler.notAuthorize();
      break;
    default:
      break;
  }

  return Promise.reject(error);
};

export default errorMiddleware;
