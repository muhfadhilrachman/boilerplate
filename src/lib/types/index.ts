interface ResponGet<T> {
  data?: {
    docs: Array<T>;
    hasNextPage?: boolean;
    hasPrevPage?: boolean;
    limit?: number;
    nextPage?: number | null;
    page?: number;
    pagingCounter?: number;
    prevPage?: null;
    totalDocs?: number;
    totalPages?: number;
  };
  code?: number | null;
  error_code?: number | null;
  errors?: any;
  message?: string;
  status?: string;
}

export type { ResponGet };
