const NAMESPACE = "AUTH";
const APP_NAME = require("../../../../package.json").name;

const ENVIRONMENT = process.env.NEXT_APP_STAGE || "local";

export const TOKEN_STORAGE_KEY = `${APP_NAME}_${ENVIRONMENT}_token`;
export const USER_STORAGE_KEY = `${APP_NAME}_${ENVIRONMENT}_user`;

export const AUTH_PROVIDER_CONFIG = {
  endpoint: {
    login: "/web-apps/super/auth",
    logout: "web-apps/dashboard-school/logout",
    // me: "web-apps/dashboard-school/auth-profile",
  },
};

export const FETCH_PROFILE = `${NAMESPACE}_FETCH_PROFILE`;
