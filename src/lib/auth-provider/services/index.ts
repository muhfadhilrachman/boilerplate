import { AxiosResponse } from "axios";

import client from "../../client";
import { AUTH_PROVIDER_CONFIG } from "../constant";
import { Login } from "@/app/login/types";
const { endpoint } = AUTH_PROVIDER_CONFIG;

function login({ email, password }: Login) {
  return client.post(endpoint.login, { email, password });
}

// function me() {
//   return client.get(endpoint.me);
// }

export { login };
