import create from "zustand";

import { userPersistance, tokenPersistance } from "../persistance";

interface User {
  _id: string;
  email: string;
  name: string;
  role: string;
  fullname: string;
  phone: string;
  isActive: boolean;
  createAt: string;
  updateAt: string;
}

interface AuthState {
  user: User;
  token: string;
  isAuth: boolean;
}

interface AuthActions {
  getAuth: () => AuthState;
  setAuth: (payload: { user: User; token: string }) => void;
  setUser: (payload: User) => void;
  clearSession: () => void;
}

interface Props extends AuthActions, AuthState {
  // ... You can add any additional properties or methods here
}

const {
  set: setUserPersistance,
  get: getUserPersistance,
  remove: removeUserPersistance,
} = userPersistance();
const {
  set: setTokenPersistance,
  get: getTokenPersistance,
  remove: removeTokenPersistance,
} = tokenPersistance();

const stateDefault: AuthState = {
  user: {
    _id: "",
    email: "",
    name: "",
    role: "",
    fullname: "",
    phone: "",
    isActive: false,
    createAt: "",
    updateAt: "",
  },
  token: "",
  isAuth: false,
};

const actions = (
  set: (fn: (state: AuthState) => AuthState) => void,
  get: () => AuthState
): AuthActions => ({
  getAuth: () => {
    const currentState = get();
    let authState = { ...currentState };

    if (!currentState.isAuth) {
      const userProfile = getUserPersistance();
      const userToken = getTokenPersistance();

      if (userProfile?.email && userToken) {
        authState = {
          user: { ...userProfile },
          token: userToken,
          isAuth: true,
        };
      }
    }

    set((state) => ({
      ...state,
      user: { ...authState.user },
      token: authState.token,
      isAuth: authState.isAuth,
    }));

    return authState;
  },

  setAuth: (payload: any) => {
    const token = payload.token.replace("Bearer ", "");
    setUserPersistance(payload);
    setTokenPersistance(token);

    set((state) => ({
      ...state,
      user: { ...payload },
      token,
      isAuth: true,
    }));
  },

  setUser: (payload: any) => {
    setUserPersistance(payload);
    set((state) => ({
      ...state,
      user: { ...payload },
    }));
  },
  clearSession: () => {
    removeUserPersistance();
    removeTokenPersistance();
    set((state) => ({
      ...state,
      ...stateDefault,
    }));
  },
});

export const useAuth = create<Props>((set, get) => ({
  ...stateDefault,
  ...actions(set, get),
}));
