import shallow from "zustand/shallow";
// import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

import { useAsync } from "../../client/hooks";

import * as authServices from "../services";
import { useAuth } from "./index";

import { Login } from "@/app/login/types";
import { useRouter } from "next/navigation";

export function useLogin() {
  const router = useRouter();
  //   const navigate = useNavigate();
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    authServices.login
  );

  const { setAuth, user } = useAuth();

  useEffect(() => {
    if (status === "resolved") {
      setAuth(value?.data);
      console.log({ value: value?.data });

      if (value?.data.role === "admin") {
        router.push("/operator/dashboard");
      }
      if (value?.data.role === "user") {
        router.push("/user/dashboard");
      }
      // router.push("/operator/dashboard");
    }
    console.log({ user });
  }, [status, value]);

  async function login(payload: Login) {
    try {
      await execute(payload);
    } catch (err) {
      console.error(err);
    }
  }

  return { login, status, value, errorMessage, fieldErrors };
}
