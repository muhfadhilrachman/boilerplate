import CookieUniversal from "universal-cookie";

const cookie = new CookieUniversal();

export const getStorage = (key: string): string | undefined => {
  return cookie.get(key);
};

export const getAllStorage = (): { [key: string]: string } => {
  return cookie.getAll();
};

export const setStorage = (key: string, value: string): void => {
  cookie.set(key, value, { path: "/" });
};

export const removeItemStorage = (key: string): void => {
  cookie.remove(key, { path: "/" });
};

export const clearStorage = (): void => {
  const cookieNames = Object.keys(cookie.getAll());
  cookieNames.forEach((cookieName) => cookie.remove(cookieName, { path: "/" }));
};
