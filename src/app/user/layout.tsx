import React from "react";
import UserLayout from "@/layouts/user-layout";

export default function layout({ children }: { children: React.ReactNode }) {
  return <UserLayout>{children}</UserLayout>;
}
