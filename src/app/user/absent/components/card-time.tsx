import React from "react";
const CardTime = () => {
  return (
    <div className="bg-white rounded-xl shadow-md p-3 text-center">
      <p className="text-rose">Clock In</p>
      <h3 className="font-semibold text-2xl">22:30</h3>
    </div>
  );
};

export default CardTime;
