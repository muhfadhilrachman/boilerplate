import { BaseButton, TextField } from "@/components/base-components";
import {
  AlertDialog,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/shacdn/ui";
import { Cross2Icon } from "@radix-ui/react-icons";
import Image from "next/image";
import React from "react";
import { useCreateAbsentEmployee } from "../context/hook";
import useAddress from "../context";

interface Props {
  isOpen: boolean;
  onClose: any;
  image: any;
  data: any;
  // location: [number, number];
}

const ModalAbsent = ({ isOpen, onClose, image, data }: Props) => {
  const { address } = useAddress();

  const date = new Date();
  const { register } = useCreateAbsentEmployee();
  // console.log({ location });

  const time = `${date.getHours()}:${date.getMinutes()}`;

  const handleAbsent = () => {
    const payload = {
      status: data.length != 0 ? "out" : "in",
      photo: image.payload,
      time: date,
      location: [6.3679401, 106.8182037],
    };
    register(payload);
  };
  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      <AlertDialogContent>
        <AlertDialogHeader className="relative">
          <AlertDialogTitle className=" ">
            {data.length != 0 ? "Absen Pulang" : "Absen Masuk"}
          </AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
        </AlertDialogHeader>
        <Image
          src={image.link}
          alt="cooo"
          width={400}
          className="rounded-xl mx-auto"
          height={200}
        />
        <div>
          <TextField label="Alamat" value={address} disabled className="" />
          <TextField label="Waktu" value={time} disabled className="mt-2" />
        </div>
        <BaseButton className="w-7/12 mx-auto" onClick={handleAbsent}>
          Submit
        </BaseButton>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default ModalAbsent;
