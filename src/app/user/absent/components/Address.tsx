"use client";
import { useState, useEffect } from "react";
import useAddress from "../context";

const Address = ({
  latitude,
  longitude,
}: {
  latitude: number;
  longitude: number;
}) => {
  const { address, setAddress } = useAddress();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${latitude}&lon=${longitude}`;
        const response = await fetch(apiUrl);
        const data = await response.json();

        if (!data.error) {
          setAddress(data.name);
        } else {
          setAddress("Tidak dapat menemukan address.");
        }
      } catch (error) {
        console.error(
          "Terjadi kesalahan dalam permintaan ke API Geocoding:",
          error
        );
        setAddress("Terjadi kesalahan dalam permintaan ke server.");
      }
    };

    fetchData();
  }, [latitude, longitude]);

  return <div>{address}</div>;
};

export default Address;
