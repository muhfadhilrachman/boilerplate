"use client";
import React, { useEffect, useState } from "react";
import CardTime from "./components/card-time";
import Webcam from "react-webcam";
import { BaseButton } from "@/components/base-components";
import ModalAbsent from "./components/modal-absent";
import { useListAbsentEmplooye } from "./context/hook";
import Address from "./components/Address";

const page = () => {
  const date = new Date();
  const [capturing, setCapturing] = useState(false);
  const [location, setLocation] = useState<any>(null);
  const { data } = useListAbsentEmplooye({
    page: 1,
    limit: 1000,
    day: "2023-10-03",
    // day: `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(
    //   2,
    //   "0"
    // )}-${String(date.getDate() + 1).padStart(2, "0")}`,
  });
  const [image, setImage] = useState<any>({
    payload: "",
    link: "",
  });
  let webcame: any = null;
  const setRef = (webcams: any) => {
    webcame = webcams;
  };
  const dataURLtoFile = (dataurl: any, filename: any) => {
    var arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  };

  const capture = () => {
    const imageSrc = webcame.getScreenshot();
    console.log(imageSrc);

    const file = dataURLtoFile(imageSrc, "preview.jpeg");
    // const audio = new Audio("./assets/camera.mp3");
    // await audio.play();
    setCapturing(true);
    setImage({
      payload: imageSrc,
      link: URL.createObjectURL(file),
    });
    // let avatar;
    // let reader = new FileReader();
    // reader.readAsDataURL(file);
    // reader.onload = function () {
    //   avatar = reader.result;
    //   // setImage(avatar);
    // };
    // reader.onerror = function (error) {
    //   console.log("Error: ", error);
    // };

    // console.log({ file, imageSrc, avatar });
  };

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLocation({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
        },
        (error) => {
          console.error(error.message);
        }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  }, []);
  console.log({ location });

  return (
    <div>
      <ModalAbsent
        isOpen={capturing}
        onClose={() => setCapturing(false)}
        image={image}
        data={data?.data?.docs || []}
        // location={[location?.longitude, location?.latitude]}
      />
      <div className="grid grid-cols-2 gap-5 mt-6">
        <CardTime />
        <CardTime />
      </div>

      <div className="px-5 py-3 my-3 text-[12px] text-center rounded-xl shadow-md text-rose font-semibold">
        {/* Jl. Bungur No.41, Waru, Surabaya */}
        {/* {location?.latitude} */}
        <Address
          latitude={location?.latitude}
          longitude={location?.longitude}
        />
      </div>
      <Webcam
        audio={false}
        screenshotFormat="image/jpeg"
        ref={setRef}
        // forceScreenshotSourceSize={true}
        mirrored={true}
        style={{ width: "100vw", borderRadius: "20px" }}
        videoConstraints={{ facingMode: "user" }}
        className="text-center my-5 "
      />
      {/* <div className="grid grid-cols-1 gap-5"> */}
      <BaseButton onClick={capture} className="w-full">
        Clock In / Clock Out
      </BaseButton>
      {/* <BaseButton>Clock Out</BaseButton> */}
      {/* </div> */}
      {/* {image && <Image src={image} alt="cooo" width={50} height={50} />} */}
    </div>
  );
};

export default page;
