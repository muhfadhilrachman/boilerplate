import { ENDPOINT } from "../constants";
import client from "../../../../lib/client";
import { CreateAbsentType } from "../types";

export function listAbsentEmployee(args: {
  page: number;
  limit: number;
  day?: string;
  idEmployee?: string;
}) {
  return client.get(ENDPOINT, {
    params: {
      ...args,
    },
  });
}

export function createAbsentEmployee(args: CreateAbsentType) {
  return client.post(ENDPOINT, {
    ...args,
  });
}
