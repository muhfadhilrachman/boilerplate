const NAMESPACE = "ABSENT";
export const ENDPOINT = "/web-apps/employee/attendance";

export const FETCH_ABSENT = `${NAMESPACE}_FETCH_ABSENT`;
