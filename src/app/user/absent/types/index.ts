interface AbsentTypes {
  createdAt: string;
  directorate: string;
  fullName: string;
  inDistance?: number | null;
  inLocationStatus?: string;
  inPhoto?: string | null;
  inStatus?: string | null;
  inTime: string | null;
  outDistance: string | null;
  outLocationStatus: string | null;
  outPhoto: string | null;
  outStatus: string | null;
  outTime: string | null;
  workingHours: string | null;
}

interface CreateAbsentType {
  location: number[];
  photo: string;
  status: string;
  time: Date;
}

export type { AbsentTypes, CreateAbsentType };
