import { useGetQuery, useAsync } from "@/lib/client/hooks";
import { ResponGet } from "@/lib/types";
import { AbsentTypes, CreateAbsentType } from "../types";
import * as TYPES from "../constants";
import * as services from "../services";

export function useListAbsentEmplooye(params: {
  page: number;
  limit: number;
  day?: string;
  idEmployee?: string;
}) {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery<
    ResponGet<AbsentTypes>
  >({
    queryKey: TYPES.ENDPOINT,
    queryFn: async () => {
      const response = await services.listAbsentEmployee(params);
      return response.data;
    },
  });
  // const datas = s
  return {
    data,
    status,
    errorMessage,
    isFetching,
    refetch,
  };
}

export function useCreateAbsentEmployee() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.createAbsentEmployee
  );

  // useEffect(() => {
  //   if (status === "resolved") {
  //     toast({
  //       title: "Success register employee",
  //       description: "Please wait...",
  //     });
  //     setResult(value);
  //   }
  //   if (status === "rejected") {
  //     toast({
  //       variant: "destructive",
  //       title: "Error register employee",
  //       description: errorMessage,
  //     });
  //   }
  // }, [status]);

  async function register(payload: CreateAbsentType) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    register,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}
