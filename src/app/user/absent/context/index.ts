import create from "zustand";

interface TypeState {
  address: string;
  setAddress: (state: any) => any;
}

const useAddress = create<TypeState>((set) => ({
  address: "",
  setAddress: (state) => set({ address: state }),
}));

export default useAddress;
