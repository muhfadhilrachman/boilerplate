import React from "react";
import { FileText } from "lucide-react";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/shacdn/ui";
import { AbsentTypes } from "../../absent/types";

interface Props {
  value: string;
  key: number;
  data: AbsentTypes;
}
const CardAttandance = ({ value, key, data }: Props) => {
  const date = new Date(data.createdAt);
  const day = date.toLocaleDateString("en-US", { weekday: "long" });
  const currentDate = date.toLocaleDateString("id-ID", {
    day: "numeric",
    month: "long",
    year: "numeric",
  });

  const outTime = new Date(data?.outTime || "");
  outTime.setUTCHours(outTime.getUTCHours() + 7);

  const absentOutTime = `${outTime.getUTCHours()}:${String(
    outTime.getUTCMinutes() + 1
  ).padStart(2, "0")}`;

  const inTime = new Date(data?.inTime || "");
  inTime.setUTCHours(inTime.getUTCHours() + 7);

  const absentInTime = `${inTime.getUTCHours()}:${String(
    inTime.getUTCMinutes() + 1
  ).padStart(2, "0")}`;
  return (
    // <Accordion>
    <AccordionItem value={value}>
      <AccordionTrigger className="mt-3 bg-white shadow-md rounded-2xl px-5 py-2 flex justify-between items-center">
        <div className=" text-left mt-2">
          <h3 className="font-bold text-[20px]">{day}</h3>
          <p className="text-xs mb-1">{currentDate}</p>
        </div>
        <div className="bg-[#E06B804D] w-10 h-10 rounded-full flex items-center justify-center">
          <FileText className="text-rose w-5 h-5" />
        </div>
      </AccordionTrigger>
      <AccordionContent className="font-semibold text-[13px]   bg-white rounded-b-2xl w-[340px] shadow-sm px-5 pt-5 mx-auto">
        {data.inTime && (
          <div className="mt-2 w-max mx-auto">
            Check in :{" "}
            <span className="text-white bg-[#ADD38D] rounded-3xl px-3 py-1">
              {absentInTime}
            </span>{" "}
            {data.inLocationStatus?.toUpperCase()}
          </div>
        )}
        {data.outTime && (
          <div className="mt-2 w-max mx-auto">
            Check out :{" "}
            <span className="text-white bg-rose rounded-3xl px-3 py-1">
              {absentOutTime}
            </span>{" "}
            {data.outLocationStatus?.toUpperCase()}
          </div>
        )}
      </AccordionContent>
    </AccordionItem>
  );
};

// <div className="mt-3 bg-white shadow-md rounded-xl px-5 py-2 flex justify-between items-center">
//   {/* <div></div> */}
//   <div className=" mt-2">
//     <p className="text-xs mb-1">23 Agustus 2023</p>
//     <h3 className="font-extrabold text-[20px]">22:30</h3>
//   </div>
//   <div className="flex flex-col items-center">
//     {/* <span className="text-xs text-black font-semibold">Check In</span> */}
//     <div className="bg-[#E06B804D] w-10 h-10 rounded-full flex items-center justify-center">
//       <FileText className="text-rose w-6 h-6"/>
//     </div>
//     {/* <div
//       className={`${
//         value === "ontime" ? "bg-[#ADD38D]" : "bg-rose"
//       } font-semibold text-[12px] text-white w-max rounded-xl py-1 px-4`}
//     >
//       {value.toUpperCase()}
//     </div> */}
//   </div>
// </div>

export default CardAttandance;
