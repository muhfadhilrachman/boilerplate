import React from "react";

const SkeletonLoading = () => {
  return (
    // <div>SkeletonLoading</div>
    <>
      <div className="mt-3 h-[62px] animate-pulse  rounded-2xl bg-slate-100"></div>
      <div className="mt-3 h-[62px] animate-pulse  rounded-2xl bg-slate-100"></div>
      <div className="mt-3 h-[62px] animate-pulse  rounded-2xl bg-slate-100"></div>
    </>
  );
};

export default SkeletonLoading;
