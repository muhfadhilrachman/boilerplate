"use client";
import React from "react";
import { Accordion } from "@/components/shacdn/ui";
import { useListAbsentEmplooye } from "../absent/context/hook";
import { useAuth } from "@/lib/auth-provider/context";
import dynamic from "next/dynamic";
const SkeletonLoading = dynamic(() => import("./components/skeleton-loading"));
const CardAttandance = dynamic(() => import("./components/card-attandance"));

const page = () => {
  const { user } = useAuth();
  const { data, isFetching } = useListAbsentEmplooye({
    page: 1,
    limit: 100,
    idEmployee: user._id,
  });

  return (
    <div>
      <p className="text-rose text-right font-semibold my-5">This Month</p>
      {isFetching ? (
        <SkeletonLoading />
      ) : (
        <Accordion type="single" collapsible className="w-full">
          {data?.data?.docs.map((val, key) => {
            return <CardAttandance key={1} value={key.toString()} data={val} />;
          })}
        </Accordion>
      )}
    </div>
  );
};

export default page;
