// "use client";
import Image from "next/image";
import logo from "../../assets/logo.svg";

export default function Loading() {
  return (
    <div className="bg-white backdrop-blur-lg bg-opacity-5 h-screen  absolute top-0 left-0 right-0 bottom-0  w-screen   flex justify-center items-center">
      <div>
        <Image src={logo} alt="logo" />
        <span>Please wait...</span>
      </div>
    </div>
  );
}
