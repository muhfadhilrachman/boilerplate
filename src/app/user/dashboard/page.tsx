"use client";
// import General from "@/layouts/general";
import React from "react";
import { MiniStatic } from "./components";

const page = () => {
  return (
    <div className="">
      <p className="text-rose text-right font-semibold my-5">This Month</p>
      <div className="grid grid-cols-1 gap-y-10">
        <MiniStatic
          className="bg-[#93D7D3] text-white"
          tittle="DIRECTORATE"
          value={20}
        />
        <MiniStatic
          className="bg-rose text-white"
          tittle="EMPLOYEE"
          value={20}
        />

        <MiniStatic
          className="bg-[#F9B775] text-white"
          tittle="MPO"
          value={20}
        />
        <MiniStatic
          className="bg-[#BBDCA0] text-white"
          tittle="BPO"
          value={20}
        />
      </div>
    </div>
  );
};

export default page;
