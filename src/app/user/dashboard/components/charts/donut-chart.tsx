import React from "react";
import ReactApexChart from "react-apexcharts";

interface Options {
  label: string;
  color: string;
}

interface Props {
  options: Options[];
}

const DonutChart = ({ options }: Props) => {
  const colors = options.map((val) => val.color);
  const labels = options.map((val) => val.label);

  return (
    <ReactApexChart
      options={{
        labels,
        dataLabels: {
          enabled: false,
        },
        colors,
        plotOptions: {
          pie: {
            donut: {
              size: "70%",
            },
            //   minAngleToShowLabel: 1,
          },
        },
        legend: {
          show: false,
        },

        responsive: [
          {
            breakpoint: 300,
            options: {
              chart: {
                width: 100,
              },
              legend: {
                position: "bottom",
              },
            },
          },
        ],
      }}
      series={[12, 31, 41, 12]}
      type="donut"
      width="270"
    />
  );
};

export default DonutChart;
