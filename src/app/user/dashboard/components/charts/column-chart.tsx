import ReactApexChart, { Props } from "react-apexcharts";

const ColumnChart = () => {
  const series = [
    {
      data: [21, 22, 10, 28, 16, 21, 13, 30],
    },
  ];
  const colors = [
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
    "#E06B80",
  ];

  return (
    <div id="chart">
      <ReactApexChart
        options={{
          series,
          chart: {
            height: 350,
            type: "bar",
          },
          colors,
          grid: {
            show: false, // Menghapus garis grid horizontal dan vertikal
          },
          plotOptions: {
            bar: {
              columnWidth: "40%",
              distributed: true,
              borderRadius: 5,
            },
          },
          dataLabels: {
            enabled: false,
          },

          legend: {
            show: false,
          },
          xaxis: {
            axisBorder: {
              show: true,
            },
            categories: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "June",
              "July",
              "Aug",
            ],
            labels: {
              style: {
                colors: "",
                fontSize: "10px",
              },
            },
          },
          yaxis: {
            axisBorder: {
              show: true,
            },
          },
        }}
        series={series}
        type="bar"
        height={250}
      />
    </div>
  );
};

export default ColumnChart;
