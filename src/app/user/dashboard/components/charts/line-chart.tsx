import ReactApexChart, { Props } from "react-apexcharts";

const LineChart = () => {
  const series = [
    {
      name: "series1",
      data: [31, 40, 28, 51, 42, 109, 100],
    },
    {
      name: "series2",
      data: [11, 32, 45, 32, 34, 52, 41],
    },
  ];

  const options: Props = {
    chart: {
      height: 350,
      type: "area",
    },
    grid: {
      show: false, // Menghapus garis grid horizontal dan vertikal
    },
    colors: ["#ADD38D", "#E06B80"],
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    xaxis: {
      axisBorder: {
        show: true,
      },
      categories: [
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu",
        "Minggu",
      ],
    },
    yaxis: {
      axisBorder: {
        show: true,
      },
    },
  };

  return (
    <div id="chart">
      <ReactApexChart
        options={options}
        series={series}
        type="area"
        height={250}
      />
    </div>
  );
};

export default LineChart;
