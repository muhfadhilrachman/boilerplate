import React from "react";
import { ColumnChart } from "./charts";

const AttandanceColumnOverview = () => {
  return (
    <div className="bg-white px-3 py-4 rounded-xl">
      <div className="flex mb-3 px-4 justify-between">
        <span className="font-semibold">Attandances</span>
        <small>Last Week</small>
      </div>
      <ColumnChart />
    </div>
  );
};

export default AttandanceColumnOverview;
