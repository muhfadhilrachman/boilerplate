import AttandanceOverview from "./attandance-line-overview";
import AttandanceColumnOverview from "./attandance-column-overview";
import TotalUserOverView from "./total-user-overview";
import MiniStatic from "./mini-static";
import ListLastAttandances from "./list-last-attandances";

export {
  AttandanceOverview,
  TotalUserOverView,
  AttandanceColumnOverview,
  MiniStatic,
  ListLastAttandances,
};
