import React from "react";
import { DonutChart } from "./charts";

const totalUseroverView = () => {
  const legend = [
    {
      label: "EMPLOYEE",
      color: "#38B2AC",
    },
    {
      label: "MPO",
      color: "#0000001A",
    },
    {
      label: "BPO",
      color: "#0088CC",
    },
    {
      label: "DIRECTORATE",
      color: "#66B2FF",
    },
  ];

  const data = [10, 20, 30, 12];
  return (
    <div className="bg-white px-3 pb-4 pt-5   rounded-xl h-max">
      <div className="flex mb-3 px-4 justify-between">
        <span className="font-semibold text-sm">Attandances</span>
        <small>Last Week</small>
      </div>
      <div className="flex items-center justify-around h-full ">
        <DonutChart options={legend} />
        <div className="pr-4">
          <div className="flex justify-between">
            <span className="font-semibold">Total</span>
            <span className="font-semibold">12.000</span>
          </div>
          {legend.map((val, key) => {
            return (
              <div key={key} className="flex  justify-between  mt-2">
                <div className="flex   mt-2">
                  <div className="flex items-center mr-6">
                    <div className={`h-4 w-4 rounded-full bg-[#38B2AC]`}></div>
                    <span className="tracking-widest ml-2 font-light text-xs">
                      {val.label}
                    </span>
                  </div>
                </div>
                <span className="font-bold">{data[key]}</span>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default totalUseroverView;
