import React from "react";
import { LineChart } from "./charts";

const AttandanceOverview = () => {
  return (
    <div className="bg-white px-3 pb-4 pt-5 rounded-xl">
      <div className="flex mb-3 px-4 justify-between">
        <span className="font-semibold text-sm">Attandances</span>
        <small>Last Week</small>
      </div>
      <LineChart />
    </div>
  );
};

export default AttandanceOverview;
