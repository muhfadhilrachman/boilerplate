interface QueryParam {
  page?: number;
  limit?: number;
  directorate?: string;
}

interface List {
  page: number;
  limit: number;
  directorate: string;
}

export type { QueryParam };
