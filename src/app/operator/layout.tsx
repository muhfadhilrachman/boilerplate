import React from "react";
import General from "@/layouts/general";

export default function layout({ children }: { children: React.ReactNode }) {
  return <General>{children}</General>;
}
