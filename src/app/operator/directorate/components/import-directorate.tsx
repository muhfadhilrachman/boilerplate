import React, { useCallback, useState } from "react";
import dragIcon from "../../../assets/drag-icon.svg";
import Image from "next/image";
import { BaseButton } from "@/components/base-components";
import { useDropzone } from "react-dropzone";
import * as XLSX from "xlsx";
import { UploadCloud } from "lucide-react";
import { DirectorateType } from "../types";
import { useImportEmployee } from "../../employee/context/hook";

interface StudentData {
  nisn: string;
  nis: string;
  // Add other fields here as needed
}

const ImportEmployee = () => {
  const [studentImport, setStudentImport] = useState<DirectorateType[]>([]);
  const { importData, status } = useImportEmployee();

  const handleFile = useCallback((file: File) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = e.target?.result as ArrayBuffer;
      const workbook = XLSX.read(data, { type: "binary" });
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      const dataParse = XLSX.utils.sheet_to_json(sheet);
      const finalyData = dataParse?.map((val: any) => {
        return {
          ...val,
          nisn: val?.nisn?.toString(),
          nis: val?.nis?.toString(),
        };
      }) as DirectorateType[];
      setStudentImport(finalyData);
    };
    reader.readAsBinaryString(file);
  }, []);

  const { getRootProps, getInputProps, isDragActive, acceptedFiles } =
    useDropzone({
      onDrop: (acceptedFiles) => {
        if (acceptedFiles && acceptedFiles[0]) {
          handleFile(acceptedFiles[0]);
        }
      },
      maxFiles: 1,
    });

  const files = acceptedFiles.map((file) => (
    <p className="text-sm text-center">
      {file.name} - {file.size} bytes
    </p>
  ));

  const handleUpload = () => {
    // importData(studentImport);
  };
  return (
    <div className="flex flex-col justify-center items-center">
      <div
        {...getRootProps()}
        className={`hover:cursor-pointer w-full ${
          isDragActive && "bg-neutral-200"
        } flex flex-col justify-center items-center hover:bg-neutral-200 rounded-xl py-5`}
      >
        <input {...getInputProps()} />

        {/* <Image src={dragIcon} alt="cooo" /> */}
        <UploadCloud className="h-24 w-24 shrink-0 opacity-30 mb-5" />
        <p className="text-sm text-center px-8">
          {isDragActive
            ? "Lepaskan file disini..."
            : "Silahkan pilih file excel yang berisi data shifting pegawai Drag drop file excel disini, atau klik untuk memilih file"}
        </p>
        {files && files}
      </div>
      <BaseButton
        className="mt-5 px-14"
        disabled={acceptedFiles.length === 0}
        onClick={handleUpload}
      >
        Upload
      </BaseButton>
    </div>
  );
};

export default ImportEmployee;
