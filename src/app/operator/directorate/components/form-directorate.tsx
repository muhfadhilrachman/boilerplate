import React from "react";
import { useFormik } from "formik";
import { INITIAL_VALUES, VALIDATION_SCHEMA } from "../constants";
import {
  BaseButton,
  BaseInput,
  SelectField,
  TextField,
  TimesField,
} from "@/components/base-components";
import TimeField from "react-simple-timefield";
import { useCreateDirectorate } from "../context/hook";
import { FormDirectorateType } from "../types";

interface Props {
  onClose: any;
}

const FormEmployee = ({ onClose }: Props) => {
  const { create, status } = useCreateDirectorate();
  const formik = useFormik({
    initialValues: INITIAL_VALUES,
    validationSchema: VALIDATION_SCHEMA,
    onSubmit: async (val: FormDirectorateType) => {
      const payload = {
        absenceDistance: val.absenceDistance,
        inTime: val.inTime,
        location: [val.latitude, val.longitude],
        name: val.name,
        outTime: val.outTime,
      };
      await create(payload);
      onClose();
      // handleSubmit(val);
    },
  });
  return (
    <form action="" onSubmit={formik.handleSubmit}>
      <TextField
        label="Nama Direktorat"
        placeholder="nama..."
        onChange={formik.handleChange}
        value={formik.values.name}
        name="name"
        className="mb-4"
        isInvalid={!!formik.errors.name && !!formik.touched.name}
        errorMessage={formik.errors.name}
      />
      <TextField
        label="Latitude"
        type="number"
        placeholder="-7.792293402384290348"
        onChange={formik.handleChange}
        value={formik.values.latitude}
        name="latitude"
        className="mb-4"
        isInvalid={!!formik.errors.latitude && !!formik.touched.latitude}
        errorMessage={formik.errors.latitude}
      />
      <TextField
        label="Latitude"
        type="number"
        placeholder="113.1389091930409"
        onChange={formik.handleChange}
        value={formik.values.longitude}
        name="longitude"
        className="mb-4"
        isInvalid={!!formik.errors.longitude && !!formik.touched.longitude}
        errorMessage={formik.errors.longitude}
      />
      <TextField
        label="Jarak Absen"
        type="number"
        placeholder="Jarak absen ( meter )"
        onChange={formik.handleChange}
        value={formik.values.absenceDistance}
        name="absenceDistance"
        className="mb-4"
        isInvalid={
          !!formik.errors.absenceDistance && !!formik.touched.absenceDistance
        }
        errorMessage={formik.errors.absenceDistance}
      />
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-5">
        <TimesField
          className="mb-4"
          label="Jam Masuk"
          onChange={(e: any) => {
            formik.setFieldValue("inTime", e.target.value);
            formik.setFieldTouched("inTime", true);
          }}  
          isInvalid={!!formik.errors.inTime && !!formik.touched.inTime}
          errorMessage={formik.errors.inTime}
          value={formik.values.inTime}
        />

        <TimesField
          className="mb-4"
          label="Jam Keluar"
          onChange={(e: any) => {
            formik.setFieldValue("outTime", e.target.value);
            formik.setFieldTouched("outTime", true);
          }}
          isInvalid={!!formik.errors.outTime && !!formik.touched.outTime}
          errorMessage={formik.errors.outTime}
          value={formik.values.outTime}
        />

        {/* <TimeField
          input={<BaseInput />}
          // value={field.value}
          // onChange={(e) => {
          //   form.setFieldValue('inTime', e.target.value);
          // }}
        /> */}
      </div>
      {/* <SelectField
        placeholder="Direktorat"
        options={listEmployee}
        label="Age"
        className="mb-2"
        onChange={(val: string) => {
          formik.setFieldValue("directorate", val);
        }}
        value={formik.values.directorate}
        isInvalid={!!formik.errors.directorate && !!formik.touched.directorate}
        errorMessage={formik.errors.directorate}
      /> */}
      {/* <TextField
        label="Jabatan"
        placeholder="jabatan..."
        onChange={formik.handleChange}
        value={formik.values.position}
        name="position"
        className="mb-2"
        isInvalid={!!formik.errors.position && !!formik.touched.position}
        errorMessage={formik.errors.position}
      />
      <TextField
        label="Email"
        placeholder="email..."
        onChange={formik.handleChange}
        type="email"
        value={formik.values.email}
        name="email"
        className="mb-5"
        isInvalid={!!formik.errors.email && !!formik.touched.email}
        errorMessage={formik.errors.email}
      /> */}
      <div className="flex justify-center items-center">
        <BaseButton
          type="submit"
          className="px-20"
          isLoading={status === "fetching"}
        >
          Submit
        </BaseButton>
      </div>
    </form>
  );
};

export default FormEmployee;
