import {
  AlertDialog,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  useToast,
} from "@/components/shacdn/ui";
import { Cross2Icon } from "@radix-ui/react-icons";
import React from "react";
import { INITIAL_VALUES, VALIDATION_SCHEMA } from "../constants";
import { useFormik } from "formik";
import {
  BaseButton,
  SelectField,
  TextField,
  TimesField,
} from "@/components/base-components";
import { FormDirectorateType } from "../types";
import { useUpdateDirectorate } from "../context/hook";
interface Props {
  isOpen: boolean;
  onClose: any;
  // tittle?: string;
  data: FormDirectorateType;
}
const EditDirectorate = ({ isOpen, onClose, data }: Props) => {
  console.log({ data });

  const { update, errorMessage, status } = useUpdateDirectorate();
  const formik = useFormik({
    initialValues: data,
    validationSchema: VALIDATION_SCHEMA,
    enableReinitialize: true,
    onSubmit: async (val) => {
      const payload = {
        absenceDistance: val.absenceDistance,
        inTime: val.inTime,
        location: [val.latitude, val.longitude],
        name: val.name,
        outTime: val.outTime,
        id: val.id,
      };
      await update(payload);
      onClose();
      // update({ ...val, id: data.__id })
      //   .then(() => {
      //     toast({
      //       title: "Success update employee",
      //       description: "Please wait...",
      //     });
      //     onClose();
      //   })
      //   .catch(() => {
      //     toast({
      //       variant: "destructive",
      //       title: "Error update employee",
      //       description: errorMessage,
      //     });
      //   });
    },
  });
  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      <AlertDialogContent>
        <AlertDialogHeader className="relative">
          <AlertDialogTitle className=" text-center">
            EDIT DIRECTORATE
          </AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
        </AlertDialogHeader>
        <AlertDialogDescription>
          <form action="" onSubmit={formik.handleSubmit}>
            <TextField
              label="Nama Direktorat"
              placeholder="nama..."
              onChange={formik.handleChange}
              value={formik.values.name}
              name="name"
              className="mb-4"
              isInvalid={!!formik.errors.name && !!formik.touched.name}
              errorMessage={formik.errors.name}
            />
            <TextField
              label="Latitude"
              type="number"
              placeholder="-7.792293402384290348"
              onChange={formik.handleChange}
              value={formik.values.latitude}
              name="latitude"
              className="mb-4"
              isInvalid={!!formik.errors.latitude && !!formik.touched.latitude}
              errorMessage={formik.errors.latitude}
            />
            <TextField
              label="Latitude"
              type="number"
              placeholder="113.1389091930409"
              onChange={formik.handleChange}
              value={formik.values.longitude}
              name="longitude"
              className="mb-4"
              isInvalid={
                !!formik.errors.longitude && !!formik.touched.longitude
              }
              errorMessage={formik.errors.longitude}
            />
            <TextField
              label="Jarak Absen"
              type="number"
              placeholder="Jarak absen ( meter )"
              onChange={formik.handleChange}
              value={formik.values.absenceDistance}
              name="absenceDistance"
              className="mb-4"
              isInvalid={
                !!formik.errors.absenceDistance &&
                !!formik.touched.absenceDistance
              }
              errorMessage={formik.errors.absenceDistance}
            />
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-5">
              <TimesField
                className="mb-4"
                label="Jam Masuk"
                onChange={(e: any) => {
                  formik.setFieldValue("inTime", e.target.value);
                  formik.setFieldTouched("inTime", true);
                }}
                isInvalid={!!formik.errors.inTime && !!formik.touched.inTime}
                errorMessage={formik.errors.inTime}
                value={formik.values.inTime}
              />

              <TimesField
                className="mb-4"
                label="Jam Keluar"
                onChange={(e: any) => {
                  formik.setFieldValue("outTime", e.target.value);
                  formik.setFieldTouched("outTime", true);
                }}
                isInvalid={!!formik.errors.outTime && !!formik.touched.outTime}
                errorMessage={formik.errors.outTime}
                value={formik.values.outTime}
              />
            </div>

            <div className="flex justify-center items-center">
              <BaseButton
                type="submit"
                className="px-20"
                isLoading={status === "fetching"}
              >
                Submit
              </BaseButton>
            </div>
          </form>
        </AlertDialogDescription>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default EditDirectorate;
