import client from "../../../../lib/client";

import { ENDPOINT } from "../constants";
import { QueryParam } from "../../../types";
import { PayloadCreateDirectorateType } from "../types";

function listDirectorate(args: QueryParam) {
  return client.get(ENDPOINT.list, {
    params: {
      ...args,
    },
  });
}

function createDirectorate(args: PayloadCreateDirectorateType) {
  return client.post(ENDPOINT.create, {
    ...args,
  });
}

function updateDirectorate(args: PayloadCreateDirectorateType) {
  return client.put(`${ENDPOINT.update}/${args.id}`, {
    ...args,
  });
}

// function deleteDirectorate(args) {
//   return client.delete(ENDPOINT.delete, {
//     params: {
//       ...args,
//     },
//   });
// }

export {
  listDirectorate,
  createDirectorate,
  updateDirectorate,
  //   createDirectorate,
  //   updateDirectorate,
  //   deleteDirectorate,
};
