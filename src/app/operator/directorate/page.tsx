"use client";
// import General from "@/layouts/general";
import React, { useEffect, useMemo, useState } from "react";
import { ListBulletIcon, PlusCircledIcon } from "@radix-ui/react-icons";
import {
  DataTable,
  SelectSearch,
  BaseInput,
  BaseButton,
  Pagination,
  BaseSelect,
  InputSearch,
  ButtonAction,
  DataError,
} from "@/components/base-components";
import { useListDirectorate } from "./context/hook";
// import AddEmployee from "./components/add-employee";
import { DirectorateType } from "./types";
import AddDirectorate from "./components/add-employee";
import useDirectorate from "./context";
import EditDirectorate from "./components/edit-directorate";

const page = () => {
  const [detail, setDetail] = useState<any>("");
  const [param, setParam] = useState({
    page: 1,
    limit: 10,
    directorate: "",
  });
  const [show, setShow] = useState({ add: false, update: false });
  const { result } = useDirectorate();
  console.log({ result });

  const {
    data: response,
    status,
    isFetching,
    refetch,
  } = useListDirectorate(param);

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Lokasi",
        accessor: "location",
      },
      {
        Header: "Jarak Absen",
        accessor: "absenceDistance",
      },
      {
        Header: "action",
        accessor: (row: DirectorateType) => {
          return (
            <ButtonAction className="text-sm">
              <div
                className="hover:cursor-pointer"
                onClick={() => {
                  setDetail({
                    absenceDistance: row.absenceDistance,
                    inTime: row.inTime,
                    longitude: row.location[1],
                    latitude: row.location[0],
                    name: row.name,
                    outTime: row.outTime,
                    id: row._id,
                  });
                  setShow({ ...show, update: true });
                }}
              >
                Edit Directorate
              </div>
              <div className="text-red-500 mt-2 hover:cursor-pointer font-semibold">
                Delete Directorate
              </div>
            </ButtonAction>
          );
        },
      },
    ],
    [response]
  );
  useEffect(() => {
    refetch();
  }, [param, result]);
  return (
    <div>
      <AddDirectorate
        isOpen={show.add}
        onClose={() => setShow({ ...show, add: false })}
      />
      <EditDirectorate
        isOpen={show.update}
        onClose={() => setShow({ ...show, update: false })}
        data={detail}
      />
      <div className="flex justify-between items-center mb-4">
        <div className="flex justify-center items-center">
          <InputSearch
            placeholder="Search directorate..."
            className="w-max"
            onChange={(e) =>
              setParam({ ...param, directorate: e.target.value })
            }
          />

          <BaseSelect
            placeholder="Entry"
            variant="rose"
            className="w-max mx-2"
            options={[
              // { label: "Entry", value: 10 },
              { label: "10 Entry", value: 10 },
              { label: "30 Entry", value: 30 },
              { label: "40 Entry", value: 40 },
            ]}
            onChange={(val: number) => {
              setParam({ ...param, limit: val });
              setParam({ ...param, limit: val, page: 1 });
            }}
            value={param.limit}
          />
        </div>

        <BaseButton onClick={() => setShow({ ...show, add: true })}>
          <PlusCircledIcon className="mr-2" /> Add Directorate
        </BaseButton>
      </div>
      <div className="bg-white px-3 py-3 rounded-lg">
        {status === "error" ? (
          <DataError refetch={refetch} />
        ) : (
          <DataTable
            data={response?.data?.docs || []}
            columns={columns}
            isLoaded={!isFetching}
            currentPage={param.page}
            limit={param.limit}
          />
        )}

        {status === "success" && (
          <Pagination
            currentPage={param.page}
            pageCount={response?.data?.totalPages || 0}
            totalItems={response?.data?.totalDocs || 0}
            maxVisible={3}
            limit={param.limit}
            gotoPage={(p) => {
              setParam({ ...param, page: p });
            }}
            previousPage={() => {
              setParam({ ...param, page: param.page - 1 });
            }}
            nextPage={() => {
              setParam({ ...param, page: param.page + 1 });
            }}
          />
        )}
      </div>
    </div>
  );
};

export default page;
