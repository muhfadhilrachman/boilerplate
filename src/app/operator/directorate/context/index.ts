import create from "zustand";

interface TypeState {
  result: any;
  setResult: (state: any) => any;
}

const useDirectorate = create<TypeState>((set) => ({
  result: {},
  setResult: (state) => set({ result: state }),
}));

export default useDirectorate;
