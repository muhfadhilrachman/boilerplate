import { useAsync, useGetQuery } from "../../../../lib/client/hooks";

import * as services from "../services";
import * as TYPES from "../constants";
import { QueryParam } from "../../../types";
import { ResponGet } from "@/lib/types";
import { DirectorateType, PayloadCreateDirectorateType } from "../types";
import { useToast } from "@/components/shacdn/ui";
import { useEffect } from "react";
import useEmployee from "../../employee/context";

export function useListDirectorate(params: QueryParam) {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery<
    ResponGet<DirectorateType>
  >({
    queryKey: TYPES.FETCH_DIRECTORATE,
    queryFn: async () => {
      const response = await services.listDirectorate(params);
      return response.data;
    },
  });

  return {
    data,
    status,
    errorMessage,
    refetch,
    isFetching,
  };
}

export function useCreateDirectorate() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.createDirectorate
  );
  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success register directorate",
        description: "Please wait...",
      });
      setResult(value?.data);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error register directorate",
        description: errorMessage,
      });
    }
  }, [status]);

  async function create(payload: PayloadCreateDirectorateType) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    create,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}

export function useUpdateDirectorate() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.updateDirectorate
  );
  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success update directorate",
        description: "Please wait...",
      });

      setResult(value?.data);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error update directorate",
        description: errorMessage,
      });
    }
  }, [status]);
  async function update(payload: PayloadCreateDirectorateType) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    update,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}

// export function useDeleteDirectorate() {
//   const { execute, status, value, errorMessage, fieldErrors } = useAsync(
//     services.deleteDirectorate
//   );

//   async function remove(payload) {
//     try {
//       await execute(payload);
//     } catch (error) {
//       console.log(error);
//       throw error;
//     }
//   }

//   return {
//     remove,
//     status,
//     value,
//     errorMessage,
//     fieldErrors,
//   };
// }
