import * as Yup from "yup";

const NAMESPACE = "DIRECTORATE";

export const ENDPOINT = {
  list: "/web-apps/company/directorate-management",
  create: "/web-apps/company/directorate-management",
  update: "/web-apps/company/directorate-management",
  delete: "/web-apps/company/directorate-management",
};

export const INITIAL_VALUES = {
  name: "",
  longitude: "",
  latitude: "",
  absenceDistance: 0,
  inTime: "",
  outTime: "",
};

export const VALIDATION_SCHEMA = Yup.object().shape({
  name: Yup.string().required("Name must be filled"),
  longitude: Yup.string().required("Longitude must be filled"),
  latitude: Yup.string().required("Latitude must be filled"),
  absenceDistance: Yup.number().required("Absence Distance must be filled"),
  inTime: Yup.string().required("inTime Distance must be filled"),
  outTime: Yup.string().required("outTime Distance must be filled"),
});

export const FETCH_DIRECTORATE = `${NAMESPACE}_FETCH_DIRECTORATE`;
export const POST_DIRECTORATE = `${NAMESPACE}_POST_DIRECTORATE`;
export const PUT_DIRECTORATE = `${NAMESPACE}_PUT_DIRECTORATE`;
export const DELETE_DIRECTORATE = `${NAMESPACE}_DELETE_DIRECTORATE`;
