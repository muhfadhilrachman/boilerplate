interface DirectorateType {
  _id: string;
  inTime: string;
  name: string;
  outTime: number;
  location: string[];
  absenceDistance: number;
  createdAt: Date; // Tipe data untuk createdAt
  updatedAt: Date;
  deletedAt?: Date;
  isActive: boolean;
}

interface PayloadCreateDirectorateType {
  absenceDistance: number;
  inTime: string;
  location: string[];
  name: string;
  outTime: string;
  id?: string;
}

interface FormDirectorateType {
  absenceDistance: number;
  inTime: string;
  longitude: string;
  latitude: string;
  name: string;
  outTime: string;
  id?: string;
}

export type {
  DirectorateType,
  PayloadCreateDirectorateType,
  FormDirectorateType,
};
