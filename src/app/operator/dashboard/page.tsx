"use client";
// import General from "@/layouts/general";
import React from "react";
import {
  AttandanceOverview,
  MiniStatic,
  AttandanceColumnOverview,
  TotalUserOverView,
  ListLastAttandances,
} from "./components";
import dynamic from "next/dynamic";
// const AttandanceColumnOverview = dynamic(() => import("./components/attandance-column-overview"));

const page = () => {
  return (
    // <General>
    <div>
      <div className="grid grid-cols-1 md:gap-y-11 md:grid-cols-2 xl:grid-cols-5 gap-5">
        <MiniStatic
          className="bg-rose text-white"
          tittle="EMPLOYEE"
          value={20}
        />
        <MiniStatic
          className="bg-[#93D7D3] text-white"
          tittle="DIRECTORATE"
          value={20}
        />
        <MiniStatic
          className="bg-[#F9B775] text-white"
          tittle="MPO"
          value={20}
        />
        <MiniStatic
          className="bg-[#BBDCA0] text-white"
          tittle="BPO"
          value={20}
        />
        <MiniStatic
          className="bg-white text-black xl:col-span-1 md:col-span-2"
          tittle="23 Agustus 2023"
          value={"20:30"}
        />
      </div>
      <div className="mt-6 grid grid-cols-1 xl:grid-cols-2 gap-5">
        <AttandanceOverview />
        <AttandanceOverview />
      </div>
      <div className="mt-6 grid grid-cols-1 xl:grid-cols-2 gap-5">
        <ListLastAttandances />
        <TotalUserOverView />

        {/* <AttandanceColumnOverview /> */}
      </div>
      <div className="mt-6"></div>
    </div>
    // </General>
  );
};

export default page;
