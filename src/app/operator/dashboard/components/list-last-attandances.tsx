"use client";
import { PersonIcon } from "@radix-ui/react-icons";
import React from "react";

const ListLastAttandance = () => {
  const data = [
    {
      name: "Lorem Ipsum",
      time: "10:00",
      status: "late",
    },
    {
      name: "Lorem Ipsum",
      time: "12:00",
      status: "early",
    },
    {
      name: "Lorem Ipsum",
      time: "10:00",
      status: "late",
    },
    {
      name: "Lorem Ipsum",
      time: "10:00",
      status: "late",
    },
  ];
  return (
    <div className="bg-white px-3 pb-4 pt-5 rounded-xl">
      <div className="flex mb-3 px-3 justify-between">
        <span className="font-semibold text-sm">Attandances</span>
        <small>Today</small>
      </div>
      <div className="px-7 py-1">
        {data.map((val, key) => {
          return (
            <div key={key} className="flex justify-between mt-5">
              <div className="flex items-center ">
                <div className="w-10 h-10 bg-rose rounded-full flex items-center justify-center">
                  <PersonIcon className="text-white text-sm" />
                </div>
                <div className="ml-5">
                  <p className="text-[#282828AB] font-semibold">{val.name}</p>
                  <p className="text-[#282828AB]">Today at{val.time}</p>
                </div>
              </div>
              <span
                className={`${
                  val.status === "late" ? "text-[#ADD38D]" : "text-rose"
                }`}
              >
                {val.status}
              </span>
            </div>
          );
        })}
      </div>
      <p className="text-center mt-3 font-semibold text-sm text-rose hover:cursor-pointer">
        More Detail
      </p>
    </div>
  );
};

export default ListLastAttandance;
