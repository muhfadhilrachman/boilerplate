"use client"

import React from "react";
import { PersonIcon } from "@radix-ui/react-icons";

interface Props {
  className?: string;
  tittle: string;
  value: number | string;
}
const MiniStatic = ({ className, tittle, value }: Props) => {
  return (
    <div
      className={`${className} rounded-xl py-5  flex flex-col items-center justify-center relative`}
    >
      <div
        className={`absolute ${className} left-0 -top-6 flex justify-center items-center  w-16 h-16 rounded-full`}
      >
        <PersonIcon className="text-2xl" />
      </div>
      <span className="tracking-widest text-xs z-10">{tittle}</span>
      <span className="tracking-widest font-bold text-2xl">{value}</span>
    </div>
  );
};

export default MiniStatic;
