interface QueryParamReport {
  page: number;
  limit: number;
  directorate?: string;
  start?: string;
  end?: string;
  search?: string;
}

interface ReportType {
  countLateStatus: number;
  count_at_office: number;
  count_out_office: number;
  directorate: string[];
  fullName: string;
  idCompany: string;
  inCount: number;
  outCount: number;
  position: string;
  totalDay: number;
  _id: string;
}
export type { QueryParamReport, ReportType };
