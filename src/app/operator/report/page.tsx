"use client";
// import General from "@/layouts/general";
import React, { useEffect, useMemo, useState } from "react";
import {
  DataTable,
  BaseButton,
  Pagination,
  BaseSelect,
  InputSearch,
  DataError,
} from "@/components/base-components";
import { useListReport } from "./context/hook";
import { QueryParamReport, ReportType } from "./types";
import { DownloadCloud } from "lucide-react";
import { utils, writeFile } from "xlsx";
import { useListDirectorate } from "../directorate/context/hook";

const page = () => {

  const [param, setParam] = useState<QueryParamReport>({
    page: 1,
    limit: 10,
    directorate: "",
    search: "",
  });
  
  const { data: response, isFetching, refetch, status } = useListReport(param);

  const { data: responseDirectorate } = useListDirectorate({ limit: 1000 });

  const listDirectoreate = responseDirectorate?.data?.docs.map((val) => ({
    value: val.name,
    label: val.name,
  }));

  const handleExport = () => {
    const wb = utils.book_new();
    const data = response?.data?.docs || [];
    const dataExport = data.map((item) => {
      return {
        Nama: item.fullName,
        Direktorat: item.directorate?.[0],
        "Attendance(Day)": item.totalDay,
        "Absen Masuk": item.inCount,
        "Absen Pulang": item.outCount,
        "Absen Dalam Kantor": item.count_at_office,
        "Absen Diluar Kantor": item.count_out_office,
        "Absen Terlambat": item.countLateStatus,
      };
    });
    const ws = utils.json_to_sheet(dataExport);
    utils.book_append_sheet(wb, ws, "Report");
    writeFile(wb, `Report.xlsx`);
  };

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "fullName",
      },
      {
        Header: "Direktorat",
        accessor: (row: any) => {
          return <>{row?.directorate[0]}</>;
        },
      },
      {
        Header: "Attandance (day)",
        accessor: "totalDay",
      },
      {
        Header: "Absen Masuk",
        accessor: "inCount",
      },
      {
        Header: "Absen Keluar",
        accessor: "outCount",
      },
      {
        Header: "Dalam Kantor",
        accessor: "count_at_office",
      },
      {
        Header: "Diluar Kantor",
        accessor: "count_out_office",
      },
      {
        Header: "Terlambat",
        accessor: "countLateStatus",
      },
    ],
    [response]
  );

  useEffect(() => {
    refetch();
  }, [param]);
  return (
    <div>
      <div className="flex justify-between items-center mb-4">
        <div className="flex ">
          {/* <BaseInput placeholder="Search employee..." className="w-max " /> */}
          <InputSearch
            placeholder="Cari nama..."
            className="w-max"
            onChange={(e) => setParam({ ...param, search: e.target.value })}
          />
          {/* <div className="flex items-center ml-2">
            <BaseInput type="date" className="bg-rose text-white " />
            <span className="text-rose mx-3">To</span>
            <BaseInput type="date" className="bg-rose text-white " />
          </div> */}
        </div>
        <div className="flex">
          <BaseSelect
            placeholder="Entry"
            variant="rose"
            className="w-max mx-1 "
            options={[
              // { label: "Entry", value: 10 },
              { label: "10 Entry", value: 10 },
              { label: "30 Entry", value: 30 },
              { label: "40 Entry", value: 40 },
            ]}
            onChange={(val: number) => {
              setParam({ ...param, limit: val, page: 1 });
            }}
            value={param.limit}
          />
          <BaseSelect
            placeholder="Directorate"
            variant="rose"
            className="w-max mx-2"
            options={listDirectoreate || []}
            onChange={(val: string) => {
              setParam({ ...param, directorate: val });
            }}
            value={param.directorate}
          />
        </div>

        <BaseButton onClick={handleExport} disabled={isFetching}>
          <DownloadCloud className="w-5 h-5 mr-3" /> Export To Excel
        </BaseButton>
      </div>
      <div className="bg-white px-3 py-2 rounded-lg">
        {status === "error" ? (
          <DataError refetch={refetch} />
        ) : (
          <DataTable<ReportType>
            data={response?.data?.docs || []}
            columns={columns}
            isLoaded={!isFetching}
            currentPage={param.page}
            limit={param.limit}
          />
        )}
        {status === "success" && (
          <Pagination
            currentPage={param.page}
            pageCount={response?.data?.totalPages || 0}
            totalItems={response?.data?.totalDocs || 0}
            maxVisible={3}
            limit={param.limit}
            gotoPage={(p) => {
              setParam({ ...param, page: p });
            }}
            previousPage={() => {
              setParam({ ...param, page: param.page - 1 });
            }}
            nextPage={() => {
              setParam({ ...param, page: param.page + 1 });
            }}
          />
        )}
      </div>
    </div>
  );
};

export default page;
