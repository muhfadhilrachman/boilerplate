import { useAsync, useGetQuery } from "../../../../lib/client/hooks";
import { QueryParamReport, ReportType } from "../types";
import * as services from "../services";
import * as TYPES from "../constants";
import { ResponGet } from "@/lib/types";

export function useListReport(params: QueryParamReport) {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery<
    ResponGet<ReportType>
  >({
    queryKey: TYPES.FETCH_REPORT,
    queryFn: async () => {
      const response = await services.listReport(params);
      return response.data;
    },
    //   enabled: params.enabled,
  });

  return {
    data,
    status,
    errorMessage,

    refetch,
    isFetching,
  };
}
