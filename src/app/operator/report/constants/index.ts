const NAMESPACE = "REPORT";

export const ENDPOINT = {
  list: "/web-apps/company/attendance-management/accumulation",
};

export const FETCH_REPORT = `${NAMESPACE}_FETCH_REPORT`;
