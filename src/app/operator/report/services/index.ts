import client from "../../../../lib/client";

import { ENDPOINT } from "../constants";
import { QueryParamReport } from "../types";

function listReport(args: QueryParamReport) {
  return client.get(ENDPOINT.list, {
    params: {
      ...args,
    },
  });
}

export { listReport };
