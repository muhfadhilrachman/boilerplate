import * as Yup from "yup";
const NAMESPACE = "EMPLOYEE";
import { CreateEmployeeType } from "../types";
export const ENDPOINT = {
  list: "/web-apps/company/employee-management",
  create: "/web-apps/company/employee-management",
  update: "/web-apps/company/employee-management",
  delete: "/web-apps/company/employee-management",
  directorate: "/web-apps/company/directorate-management",
  import: "/web-apps/company/employee-management/import",
  checkDuplicate: "/web-apps/company/employee-management/check-duplicate",
};

export const INITIAL_VALUES: CreateEmployeeType = {
  fullName: "",
  directorate: "",
  nik: "",
  email: "",
  position: "",
};

export const VALIDATION_SCHEMA = Yup.object().shape({
  fullName: Yup.string().required("Full Name must be filled"),
  directorate: Yup.string().required("Directorate must be filled"),
  nik: Yup.string().required("NIK must be filled"),
  email: Yup.string().email().required("Email must be filled"),
  position: Yup.string().required("Position must be filled"),
});

export const FETCH_EMPLOYEE = `${NAMESPACE}_FETCH_EMPLOYEE`;
export const POST_EMPLOYEE = `${NAMESPACE}_POST_EMPLOYEE`;
export const PUT_EMPLOYEE = `${NAMESPACE}_PUT_EMPLOYEE`;
export const DELETE_EMPLOYEE = `${NAMESPACE}_DELETE_EMPLOYEE`;
export const FETCH_DIRECTORATE = `${NAMESPACE}_FETCH_DIRECTORATE`;
