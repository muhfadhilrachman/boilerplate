import { useGetQuery, useAsync } from "@/lib/client/hooks";
import { CreateEmployeeType, QueryParamsEmplooye } from "../types";
import * as TYPES from "../constants";
import * as services from "../services";
import { EmplooyeType } from "../types";
import { ResponGet } from "@/lib/types";
import { useEffect } from "react";
import useEmployee from ".";
import { useToast } from "@/components/shacdn/ui";

export function useListEmplooye(params: QueryParamsEmplooye) {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery<
    ResponGet<EmplooyeType>
  >({
    queryKey: TYPES.FETCH_DIRECTORATE,
    queryFn: async () => {
      const response = await services.listEmployee(params);
      return response.data;
    },
  });
  // const datas = s
  return {
    data,
    status,
    errorMessage,
    isFetching,
    refetch,
  };
}
export function useRegisterEmployee() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.createEmployee
  );
  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success register employee",
        description: "Please wait...",
      });
      setResult(value);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error register employee",
        description: errorMessage,
      });
    }
  }, [status]);

  async function register(payload: CreateEmployeeType) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    register,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}

export function useDeleteEmployee() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.deleteEmployee
  );
  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success delete employee",
        description: "Please wait...",
      });
      setResult(value || Math.random());
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error delete employee",
        description: errorMessage,
      });
    }
  }, [status]);

  async function remove(payload: string) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    remove,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}

export function useUpdateEmployee() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.updateEmployee
  );
  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success update employee",
        description: "Please wait...",
      });
      setResult(value);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error update employee",
        description: errorMessage,
      });
    }
  }, [status]);

  async function update(payload: any) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    update,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}

export function useImportEmployee() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.importEmployee
  );

  const { setResult } = useEmployee();
  const { toast } = useToast();

  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success import employee",
        description: "Please wait...",
      });
      setResult(value);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error import employee",
        description: errorMessage,
      });
    }
  }, [status]);
  async function importData(payload: CreateEmployeeType[]) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    importData,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}
