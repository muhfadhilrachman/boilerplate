import create from "zustand";

interface TypeState {
  result: any;
  setResult: (state: any) => any;
}

const useEmployee = create<TypeState>((set) => ({
  result: {},
  setResult: (state) => set({ result: state }),
}));

export default useEmployee;
