"use client";
// import General from "@/layouts/general";
import React, { useEffect, useMemo, useState } from "react";
import { ListBulletIcon, PlusCircledIcon } from "@radix-ui/react-icons";
import {
  DataTable,
  BaseButton,
  Pagination,
  BaseSelect,
  InputSearch,
  ButtonAction,
  ModalDelete,
  DataError,
} from "@/components/base-components";
import { useDeleteEmployee, useListEmplooye } from "./context/hook";
import { useListDirectorate } from "../directorate/context/hook";
import { EmplooyeType } from "./types";
import useEmployee from "./context";
import dynamic from "next/dynamic";
const AddEmployee = dynamic(() => import("./components/add-employee"));
const EditEmployee = dynamic(() => import("./components/edit-employee"));

const page = () => {
  const { result } = useEmployee();
  const [param, setParam] = useState({
    page: 1,
    limit: 10,
    fullName: "",
    directorate: "",
  });
  const [show, setShow] = useState({ add: false, delete: false, edit: false });
  const [id, setId] = useState("");
  const [detail, setDetail] = useState<EmplooyeType>({
    _id: "",
    directorate: "",
    fullName: "",
    email: "",
    nik: "",
    position: "",
  });

  const {
    data: response,
    status,
    isFetching,
    refetch,
  } = useListEmplooye(param);
  const { remove, errorMessage } = useDeleteEmployee();
  const { data: responseDirectorate } = useListDirectorate({ limit: 1000 });

  const listDirectoreate = responseDirectorate?.data?.docs.map((val) => ({
    value: val.name,
    label: val.name,
  }));

  const listDirectoreateForm = responseDirectorate?.data?.docs.map((val) => ({
    value: val._id,
    label: val.name,
  }));

  const handleDelete = async () => {
    await remove(id);
    // .then(() => {
    //   toast({
    //     title: "Success delete employee",
    //     description: "Please wait...",
    //   });
    //   // onClose();
    // })
    // .catch(() => {
    //   toast({
    //     variant: "destructive",
    //     title: "Error delete employee",
    //     description: errorMessage,
    //   });
    // });
    setShow({ ...show, delete: false });
  };

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "fullName",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Posisi",
        accessor: "position",
      },
      {
        Header: "Perusahaan",
        accessor: "directorate",
      },
      {
        Header: "action",
        accessor: (row: EmplooyeType) => {
          return (
            <ButtonAction className="text-sm">
              <div
                className="hover:cursor-pointer"
                onClick={() => {
                  setShow({ ...show, edit: true });
                  setDetail(row);
                }}
              >
                Edit Employee
              </div>
              <div
                className="text-red-500 mt-2 hover:cursor-pointer font-semibold"
                onClick={() => {
                  setShow({ ...show, delete: true });
                  setId(row._id);
                }}
              >
                Delete Employee
              </div>
            </ButtonAction>
          );
        },
      },
    ],
    [response]
  );

  useEffect(() => {
    refetch();
  }, [param, result]);
  return (
    <div>
      <AddEmployee
        isOpen={show.add}
        onClose={() => setShow({ ...show, add: false })}
        listEmployee={listDirectoreateForm || []}
      />
      <EditEmployee
        isOpen={show.edit}
        onClose={() => setShow({ ...show, edit: false })}
        listEmployee={listDirectoreateForm || []}
        data={detail}
      />
      <ModalDelete
        isOpen={show.delete}
        onClose={() => setShow({ ...show, delete: false })}
        onSubmit={handleDelete}
      />
      <div className="flex justify-between items-center mb-4">
        <div className="flex justify-center items-center">
          {/* <BaseInput placeholder="Search employee..." className="w-max" /> */}
          <InputSearch
            placeholder="Cari nama..."
            onChange={(e) => setParam({ ...param, fullName: e.target.value })}
          />
          <BaseSelect
            placeholder="Entry"
            variant="rose"
            className="w-max mx-2"
            options={[
              // { label: "Entry", value: 10 },
              { label: "10 Entry", value: 10 },
              { label: "30 Entry", value: 30 },
              { label: "40 Entry", value: 40 },
            ]}
            onChange={(val: number) => {
              setParam({ ...param, limit: val, page: 1 });
            }}
            value={param.limit}
          />
          <BaseSelect
            placeholder="Directorate"
            variant="rose"
            className="w-max mx-2"
            options={listDirectoreate || []}
            onChange={(val: string) => {
              setParam({ ...param, directorate: val });
            }}
            value={param.directorate}
          />
          {/* <SelectSearch
            onChange={(val: any) => {
              console.log({ val });
            }}
            label={
              <>
                <ListBulletIcon className="mr-2" />
                <span>Pilih Direktorat</span>
              </>
            }
            className="mx-2"
            options={listDirectoreate || []}
          /> */}
        </div>

        <BaseButton onClick={() => setShow({ ...show, add: true })}>
          <PlusCircledIcon className="mr-2" /> Add Employee
        </BaseButton>
      </div>
      <div className="bg-white px-3 py-3 rounded-lg">
        {status === "error" ? (
          <DataError refetch={refetch} />
        ) : (
          <DataTable
            data={response?.data?.docs || []}
            columns={columns}
            isLoaded={!isFetching}
            currentPage={param.page}
            limit={param.limit}
          />
        )}
        {status === "success" && (
          <>
            <Pagination
              currentPage={param.page}
              pageCount={response?.data?.totalPages || 0}
              totalItems={response?.data?.totalDocs || 0}
              maxVisible={3}
              limit={param.limit}
              gotoPage={(p) => {
                setParam({ ...param, page: p });
              }}
              previousPage={() => {
                setParam({ ...param, page: param.page - 1 });
              }}
              nextPage={() => {
                setParam({ ...param, page: param.page + 1 });
              }}
            />
          </>
        )}
      </div>
    </div>
  );
};

export default page;
