import client from "../../../../lib/client";
import {
  QueryParamsEmplooye,
  CreateEmployeeType,
  EmplooyeType,
} from "../types";
import { ENDPOINT } from "../constants";

function listEmployee(args: QueryParamsEmplooye) {
  return client.get(ENDPOINT.list, {
    params: {
      ...args,
    },
  });
}

function createEmployee(args: CreateEmployeeType) {
  return client.post(ENDPOINT.create, {
    ...args,
  });
}

function updateEmployee(args: any) {
  console.log(args);

  return client.put(`${ENDPOINT.update}/${args.id}`, {
    ...args,
  });
}

function deleteEmployee(id: string) {
  const idEmployee = id;
  console.log({ idEmployee });

  return client.delete(
    `${ENDPOINT.delete}/${idEmployee}`
    // {
    //   id,
    // }
  );
}

// function listDirectorate(args) {
//   return client.get(ENDPOINT.directorate, {
//     params: {
//       ...args,
//     },
//   });
// }

function importEmployee(args: CreateEmployeeType[]) {
  return client.post(ENDPOINT.import, [...args]);
}

export {
  listEmployee,
  deleteEmployee,
  createEmployee,
  //   createEmployee,
  updateEmployee,
  //   deleteEmployee,
  //   listDirectorate,
  importEmployee,
};
