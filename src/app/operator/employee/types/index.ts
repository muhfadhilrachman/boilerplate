interface QueryParamsEmplooye {
  page?: number;
  limit?: number;
  directorate?: string;
  fullName?: string;
}

interface CreateEmployeeType {
  fullName: string;
  directorate: string;
  nik: string;
  email: string;
  position: string;
}

interface EmplooyeType {
  fullName: string;
  directorate: string;
  nik: string;
  email: string;
  position: string;
  _id: string;
}

export type { QueryParamsEmplooye, CreateEmployeeType, EmplooyeType };
