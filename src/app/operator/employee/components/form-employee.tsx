import React from "react";
import { useFormik } from "formik";
import { INITIAL_VALUES, VALIDATION_SCHEMA } from "../constants";
import {
  BaseButton,
  SelectField,
  TextField,
} from "@/components/base-components";
import { useRegisterEmployee } from "../context/hook";
import { useToast } from "@/components/shacdn/ui";

interface Props {
  listEmployee: Array<{ value: string; label: string }>;
  onClose: any;
}

const FormEmployee = ({ listEmployee, onClose }: Props) => {
  const { toast } = useToast();
  const { register, errorMessage, status } = useRegisterEmployee();
  const formik = useFormik({
    initialValues: INITIAL_VALUES,
    validationSchema: VALIDATION_SCHEMA,
    onSubmit: async (val) => {
      await register(val);
      onClose();
    },
  });
  return (
    <form action="" onSubmit={formik.handleSubmit}>
      <TextField
        label="Nama Lengkap"
        placeholder="nama..."
        onChange={formik.handleChange}
        value={formik.values.fullName}
        name="fullName"
        className="mb-4"
        isInvalid={!!formik.errors.fullName && !!formik.touched.fullName}
        errorMessage={formik.errors.fullName}
      />
      <TextField
        label="Nik"
        type="number"
        placeholder="nik..."
        onChange={formik.handleChange}
        value={formik.values.nik}
        name="nik"
        className="mb-4"
        isInvalid={!!formik.errors.nik && !!formik.touched.nik}
        errorMessage={formik.errors.nik}
      />
      <SelectField
        placeholder="Direktorat"
        options={listEmployee}
        label="Age"
        className="mb-4"
        onChange={(val: string) => {
          formik.setFieldValue("directorate", val);
        }}
        value={formik.values.directorate}
        isInvalid={!!formik.errors.directorate && !!formik.touched.directorate}
        errorMessage={formik.errors.directorate}
      />
      <TextField
        label="Jabatan"
        placeholder="jabatan..."
        onChange={formik.handleChange}
        value={formik.values.position}
        name="position"
        className="mb-4"
        isInvalid={!!formik.errors.position && !!formik.touched.position}
        errorMessage={formik.errors.position}
      />
      <TextField
        label="Email"
        placeholder="email..."
        onChange={formik.handleChange}
        type="email"
        value={formik.values.email}
        name="email"
        className="mb-5"
        isInvalid={!!formik.errors.email && !!formik.touched.email}
        errorMessage={formik.errors.email}
      />
      <div className="flex justify-center items-center">
        <BaseButton
          type="submit"
          className="px-20"
          isLoading={status === "fetching"}
        >
          Submit
        </BaseButton>
      </div>
    </form>
  );
};

export default FormEmployee;
