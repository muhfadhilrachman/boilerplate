import React, { useState } from "react";
import {
  AlertDialog,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/components/shacdn/ui/";
import { Cross2Icon } from "@radix-ui/react-icons";
import FormEmployee from "./form-employee";
import ImportEmployee from "./import-employee";

interface Props {
  isOpen: boolean;
  onClose: any;
  tittle?: string;
  onSubmit?: React.MouseEventHandler<HTMLButtonElement>;
  listEmployee: Array<{ value: string; label: string }>;
}

const AddEmployee = ({ isOpen, onClose, tittle, listEmployee }: Props) => {
  const [tab, setTab] = useState("manual");

  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      {/* <AlertDialogTrigger></AlertDialogTrigger> */}
      <AlertDialogContent>
        <AlertDialogHeader className="relative ">
          <AlertDialogTitle className=" text-center">
            ADD EMPLOYEE
          </AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
          <AlertDialogDescription>
            <Tabs defaultValue="manual" className="w-full ">
              <div className="flex justify-end ">
                <TabsList className="rounded-xl px-2  ">
                  <TabsTrigger
                    value="manual"
                    className={`rounded-xl text-whitepx-7`}
                    onClick={() => setTab("manual")}
                  >
                    Manual
                  </TabsTrigger>
                  <TabsTrigger
                    value="import"
                    unselectable="off"
                    className={`rounded-xl text-whitex px-7`}
                    onClick={() => setTab("import")}
                  >
                    Import
                  </TabsTrigger>
                </TabsList>
              </div>
              <TabsContent value="manual">
                <FormEmployee listEmployee={listEmployee} onClose={onClose} />
              </TabsContent>
              <TabsContent value="import">
                <ImportEmployee />
              </TabsContent>
            </Tabs>
          </AlertDialogDescription>
        </AlertDialogHeader>
        {/* <AlertDialogFooter className="flex justify-center items-center">
      
        </AlertDialogFooter> */}
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default AddEmployee;
