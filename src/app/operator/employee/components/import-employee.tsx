import React, { useCallback, useState } from "react";
import dragIcon from "../../../assets/drag-icon.svg";
import Image from "next/image";
import { BaseButton } from "@/components/base-components";
import { useDropzone } from "react-dropzone";
import * as XLSX from "xlsx";
import { UploadCloud } from "lucide-react";
import { useImportEmployee } from "../context/hook";
import { EmplooyeType } from "../types";

const ImportEmployee = () => {
  const [employee, setEmployee] = useState<EmplooyeType[]>([]);
  const { importData, status } = useImportEmployee();

  const handleFile = useCallback((file: File) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = e.target?.result as ArrayBuffer;
      const workbook = XLSX.read(data, { type: "binary" });
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      const dataParse = XLSX.utils.sheet_to_json(sheet);
      const finalyData = dataParse?.map((val: any) => {
        return {
          ...val,
          nisn: val?.nisn?.toString(),
          nis: val?.nis?.toString(),
        };
      }) as EmplooyeType[];
      setEmployee(finalyData);
    };
    reader.readAsBinaryString(file);
  }, []);

  const handleUpload = () => {
    importData(employee);
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    acceptedFiles,
    fileRejections,
  } = useDropzone({
    onDrop: (acceptedFiles) => {
      if (acceptedFiles && acceptedFiles[0]) {
        handleFile(acceptedFiles[0]);
      }
    },
    accept: {
      "text/csv": [
        ".csv, .xlsx, .xls, text/csv, application/vnd.ms-excel, application/csv, text/x-csv, application/x-csv, text/comma-separated-values, text/x-comma-separated-values",
      ],
    },
    maxFiles: 1,
  });

  const files = acceptedFiles.map((file) => (
    <p className="text-sm text-center">
      {file.name} - {file.size} bytes
    </p>
  ));

  return (
    <div className="flex flex-col justify-center items-center">
      <div
        {...getRootProps()}
        className={`hover:cursor-pointer w-full ${
          isDragActive && "bg-neutral-200"
        } flex flex-col justify-center items-center hover:bg-neutral-200 rounded-xl py-5`}
      >
        <input {...getInputProps()} accept=".xlsx, .xls, .csv" />

        {/* <Image src={dragIcon} alt="cooo" /> */}
        <UploadCloud className="h-24 w-24 shrink-0 opacity-30 mb-5" />

        <p className="text-sm text-center px-8">
          {isDragActive
            ? "Lepaskan file disini..."
            : "Silahkan pilih file excel yang berisi data shifting pegawai Drag drop file excel disini, atau klik untuk memilih file"}
        </p>
        {files && fileRejections.length > 0 ? (
          <p className="text-red-500 mt-2">
            Hanya file Excel (CSV, XLS, XLSX) yang diperbolehkan!
          </p>
        ) : (
          files
        )}
        {/* {fileRejections.length > 1 && (
          <p className="text-red-500">
            Hanya file Excel (CSV, XLS, XLSX) yang diperbolehkan!
          </p>
        )} */}
      </div>
      <BaseButton
        className="mt-5 px-14"
        onClick={handleUpload}
        isLoading={status === "fetching"}
        disabled={acceptedFiles.length === 0}
      >
        Upload
      </BaseButton>
    </div>
  );
};

export default ImportEmployee;
