import {
  AlertDialog,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  useToast,
} from "@/components/shacdn/ui";
import { Cross2Icon } from "@radix-ui/react-icons";
import React from "react";
import { INITIAL_VALUES, VALIDATION_SCHEMA } from "../constants";
import { useFormik } from "formik";
import {
  BaseButton,
  SelectField,
  TextField,
} from "@/components/base-components";
import { EmplooyeType } from "../types";
import { useUpdateEmployee } from "../context/hook";
interface Props {
  isOpen: boolean;
  onClose: any;
  // tittle?: string;
  data: EmplooyeType;
  // onSubmit?: React.MouseEventHandler<HTMLButtonElement>;
  listEmployee: Array<{ value: string; label: string }>;
}
const EditEmployee = ({ isOpen, onClose, data, listEmployee }: Props) => {
  console.log({ data });
  const { toast } = useToast();

  const { update, errorMessage, status } = useUpdateEmployee();
  const formik = useFormik({
    initialValues: data,
    validationSchema: VALIDATION_SCHEMA,
    enableReinitialize: true,
    onSubmit: async (val) => {
      await update({ ...val, id: data._id });

      onClose();
    },
  });
  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      <AlertDialogContent>
        <AlertDialogHeader className="relative">
          <AlertDialogTitle className=" text-center">
            EDIT EMPLOYEE
          </AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
        </AlertDialogHeader>
        <AlertDialogDescription>
          <form action="" onSubmit={formik.handleSubmit}>
            <TextField
              label="Nama Lengkap"
              placeholder="nama..."
              onChange={formik.handleChange}
              value={formik.values.fullName}
              name="fullName"
              className="mb-4"
              isInvalid={!!formik.errors.fullName && !!formik.touched.fullName}
              errorMessage={formik.errors.fullName}
            />
            <TextField
              label="Nik"
              type="number"
              placeholder="nik..."
              onChange={formik.handleChange}
              value={formik.values.nik}
              name="nik"
              className="mb-4"
              isInvalid={!!formik.errors.nik && !!formik.touched.nik}
              errorMessage={formik.errors.nik}
            />
            <SelectField
              placeholder="Direktorat"
              options={listEmployee}
              label="Age"
              className="mb-4"
              onChange={(val: string) => {
                formik.setFieldValue("directorate", val);
              }}
              value={formik.values.directorate}
              isInvalid={
                !!formik.errors.directorate && !!formik.touched.directorate
              }
              errorMessage={formik.errors.directorate}
            />
            <TextField
              label="Jabatan"
              placeholder="jabatan..."
              onChange={formik.handleChange}
              value={formik.values.position}
              name="position"
              className="mb-4"
              isInvalid={!!formik.errors.position && !!formik.touched.position}
              errorMessage={formik.errors.position}
            />
            <TextField
              label="Email"
              placeholder="email..."
              onChange={formik.handleChange}
              type="email"
              value={formik.values.email}
              name="email"
              className="mb-5"
              isInvalid={!!formik.errors.email && !!formik.touched.email}
              errorMessage={formik.errors.email}
            />
            <div className="flex justify-center items-center">
              <BaseButton
                type="submit"
                className="px-20"
                isLoading={status === "fetching"}
              >
                Submit
              </BaseButton>
            </div>
          </form>
        </AlertDialogDescription>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default EditEmployee;
