const NAMESPACE = "ATTENDANCE";

export const ENDPOINT = {
  list: "/web-apps/company/attendance-management",
  directorate: "/web-apps/company/directorate-management",
};

export const FETCH_ATTENDANCE = `${NAMESPACE}_FETCH_ATTENDANCE`;
export const FETCH_ATTENDANCEMIX = `${NAMESPACE}_FETCH_ATTENDANCEMIX`;
export const FETCH_DIRECTORATE = `${NAMESPACE}_FETCH_DIRECTORATE`;
