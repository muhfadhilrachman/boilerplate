import client from "../../../../lib/client";

import { ENDPOINT } from "../constants";
import { QueryParamAttandance } from "../types";

function listAttendance(args: QueryParamAttandance) {
  return client.get(ENDPOINT.list, {
    params: {
      ...args,
    },
  });
}

// function listDirectorate(args) {
//   return client.get(ENDPOINT.directorate, {
//     params: {
//       ...args,
//     },
//   });
// }

export { listAttendance };
