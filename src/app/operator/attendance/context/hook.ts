import { useAsync, useGetQuery } from "../../../../lib/client/hooks";

import * as services from "../services";
import * as TYPES from "../constants";
import { QueryParamAttandance } from "../types";

export function useListAttendance(params: QueryParamAttandance) {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery({
    queryKey: TYPES.FETCH_ATTENDANCE,
    queryFn: async () => {
      const response = await services.listAttendance(params);
      return response.data;
    },
  });

  return {
    data,
    status,
    errorMessage,
    refetch,
    isFetching,
  };
}

// export function useListAttendanceMix(params) {
//   const { data, status, errorMessage, isIdle, refetch } = useGetQuery({
//     queryKey: TYPES.FETCH_ATTENDANCEMIX,
//     queryFn: async () => {
//       const response = await services.listAttendance(params);
//       return response.data;
//     },
//   });

//   return {
//     data,
//     status,
//     errorMessage,
//     isIdle,
//     refetch,
//   };
// }

// export function useListDirectorate(params) {
//   const { data, status, errorMessage, isIdle, refetch } = useGetQuery({
//     queryKey: TYPES.FETCH_DIRECTORATE,
//     queryFn: async () => {
//       const response = await services.listDirectorate(params);
//       return response.data;
//     },
//   });

//   return {
//     data,
//     status,
//     errorMessage,
//     isIdle,
//     refetch,
//   };
// }
