"use client";
// import General from "@/layouts/general";
import React, { useEffect, useMemo, useState } from "react";
import {
  DataTable,
  BaseInput,
  BaseButton,
  Pagination,
  BaseSelect,
  InputSearch,
  DataError,
} from "@/components/base-components";
import { useListAttendance } from "./context/hook";
import { DownloadCloud } from "lucide-react";
import { useListDirectorate } from "../directorate/context/hook";
import { utils, writeFile } from "xlsx";

const page = () => {
  const today = new Date();
  const year = today.getFullYear();
  const month = String(today.getMonth() + 1).padStart(2, "0");
  const day = String(today.getDate()).padStart(2, "0");

  const [param, setParam] = useState<any>({
    page: 1,
    limit: 10,
    directorate: "",
    search: "",
    day: `${year}-${month}-${day}`,
  });

  const {
    data: response,
    status,
    isFetching,
    refetch,
  } = useListAttendance(param);

  const { data: responseDirectorate } = useListDirectorate({ limit: 1000 });

  const listDirectoreate = responseDirectorate?.data?.docs.map((val) => ({
    value: val.name,
    label: val.name,
  }));
  const formatDateForInput = (dateString: string) => {
    const date = new Date(dateString);

    const formattedDate = date.toLocaleDateString("id-ID", {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    });
    return formattedDate;
  };
  const getHours = (inTime: string) => {
    const date = new Date(inTime);

    const formattedTime = date.toLocaleTimeString("id-ID", {
      hour: "numeric",
      minute: "numeric",
    });
    return formattedTime;
  };

  const downloadExcel = () => {
    const wb = utils.book_new();
    const data = response?.data?.docs || [];
    const dataExport = data.map((item: any) => {
      return {
        Nama: item?.fullName,
        Kelas: item?.rombel?.name,
        tanggal: item?.date,
        "Waktu Masuk": item?.inTime && getHours(item?.inTime),
        "Status Masuk": item?.inStatus
          ? item?.inStatus === "late"
            ? "Telat"
            : "Tepat Waktu"
          : null,
        "Waktu Keluar": item?.outTime && getHours(item?.outTime),
        "Status Keluar": item?.outStatus
          ? item?.outStatus === "early"
            ? "Pulang Lebih Awal"
            : "Tepat Waktu"
          : null,
      };
    });
    // console.log({ dataExport });
    const ws = utils.json_to_sheet(dataExport);
    utils.book_append_sheet(wb, ws, "Data Siswa");
    writeFile(wb, `data-riwayat-absen-siswa.xlsx`);
  };

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "fullName",
      },
      {
        Header: "Direktorat",
        accessor: "directorate",
      },
      {
        Header: "Jenis",
        accessor: (row: any) => {
          return (
            <div className="text-xs">
              {row.inStatus && (
                <p className="text-rose font-semibold mb-1">KELUAR</p>
              )}
              {row.outStatus && (
                <p className="text-[#ADD38D] font-semibold">MASUK</p>
              )}
            </div>
          );
        },
      },
      {
        Header: "Tanggal",
        accessor: (row: any) => {
          return <>{formatDateForInput(row.inTime)}</>;
        },
      },

      {
        Header: "Waktu",
        accessor: (row: any) => {
          const currentDateIntime = new Date(row?.inTime);
          const nextWeekUTC = new Date(
            currentDateIntime.getTime() + 7 * 60 * 60 * 1000
          );

          const currentDateOutTime = new Date(row?.outTime);
          const nextWeekUTCOutTime = new Date(
            currentDateOutTime.getTime() + 7 * 60 * 60 * 1000
          );

          const hour = String(nextWeekUTC.getUTCHours()).padStart(2, "0");
          const minute = String(nextWeekUTC.getUTCMinutes()).padStart(2, "0");

          const hourOutTime = String(nextWeekUTCOutTime.getUTCHours()).padStart(
            2,
            "0"
          );
          const minuteOutTime = String(
            nextWeekUTCOutTime.getUTCMinutes()
          ).padStart(2, "0");
          return (
            <div className="text-xs">
              {row.inTime && (
                <p>
                  {hour} {minute}
                </p>
              )}
              {row.outTime && (
                <p>
                  {hourOutTime} {minuteOutTime}
                </p>
              )}
            </div>
          );
        },
      },
      {
        Header: "Status Lokasi",
        accessor: (row: any) => {
          return (
            <div className="text-xs">
              {row.inStatus && (
                <p className="text-rose font-semibold mb-1">{row.inStatus}</p>
              )}
              {row.outStatus && (
                <p className="text-[#ADD38D] font-semibold">{row.outStatus}</p>
              )}
            </div>
          );
        },
      },
    ],
    [response]
  );

  useEffect(() => {
    refetch();
  }, [param]);

  return (
    // <General>
    <div>
      <div className="flex justify-between items-center mb-4">
        <div className="flex justify-center items-center">
          <InputSearch
            placeholder="Cari nama..."
            className="w-max"
            onChange={(e) => setParam({ ...param, search: e.target.value })}
          />
          <BaseSelect
            placeholder="Entry"
            variant="rose"
            className=" mx-2"
            options={[
              // { label: "Entry", value: 10 },
              { label: "10 Entry", value: 10 },
              { label: "30 Entry", value: 30 },
              { label: "40 Entry", value: 40 },
            ]}
            onChange={(val: number) => {
              setParam({ ...param, limit: val });
            }}
            value={param.limit}
          />
          <BaseInput
            type="date"
            value={param.day.toString()}
            onChange={(e) => setParam({ ...param, day: e.target.value })}
            className="bg-rose text-white"
          />
          <BaseSelect
            placeholder="Directorate"
            variant="rose"
            className="w-max mx-2"
            options={listDirectoreate || []}
            onChange={(val: string) => {
              setParam({ ...param, directorate: val });
            }}
            value={param.directorate}
          />
        </div>

        <BaseButton onClick={downloadExcel}>
          <DownloadCloud className="w-5 h-5 mr-3" /> Export To Excel
        </BaseButton>
      </div>
      <div className="bg-white px-3 py-3 rounded-lg">
        {status === "error" ? (
          <DataError refetch={refetch} />
        ) : (
          <DataTable
            data={response?.data?.docs || []}
            columns={columns}
            isLoaded={!isFetching}
            currentPage={param.page}
            limit={param.limit}
          />
        )}

        {status === "success" && (
          <Pagination
            currentPage={param.page}
            pageCount={response?.data?.totalPages || 0}
            totalItems={response?.data?.totalDocs || 0}
            maxVisible={3}
            limit={param.limit}
            gotoPage={(p) => {
              setParam({ ...param, page: p });
            }}
            previousPage={() => {
              setParam({ ...param, page: param.page - 1 });
            }}
            nextPage={() => {
              setParam({ ...param, page: param.page + 1 });
            }}
          />
        )}
      </div>
    </div>
    // </General>
  );
};

export default page;
