interface QueryParamAttandance {
  page: number;
  limit: number;
  directorate: string;
}

export type { QueryParamAttandance };
