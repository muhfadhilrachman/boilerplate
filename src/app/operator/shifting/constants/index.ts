const NAMESPACE = "SHIFTING";

export const ENDPOINT = {
  list: "/web-apps/company/shifting-management",
  import: "/web-apps/company/shifting-management",
};

export const FETCH_LIST = `${NAMESPACE}_FETCH_LIST`;
export const POST_SHIFT = `${NAMESPACE}_POST_SHIFT`;
