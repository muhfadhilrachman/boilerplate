import client from "../../../../lib/client";

import { ENDPOINT } from "../constants";

function list() {
  return client.get(ENDPOINT.list, {
    // params: {
    //   ...args,
    // },
  });
}

function importShift(args: any) {
  return client.post(ENDPOINT.import, [...args]);
}

export { list, importShift };
