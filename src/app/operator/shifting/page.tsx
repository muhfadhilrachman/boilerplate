"use client";
import Image from "next/image";
import React, { useCallback, useState } from "react";
import { useDropzone } from "react-dropzone";
import * as XLSX from "xlsx";
import dragIcon from "../../assets/drag-icon.svg";
import { BaseButton } from "@/components/base-components";
import { DownloadCloud, UploadCloud } from "lucide-react";
import { useImportShift, useListShifting } from "./context/hook";
// import { read, utils, writeFile } from "xlsx";

interface StudentData {
  nisn: string;
  nis: string;
  // Add other fields here as needed
}

const page = () => {
  const [studentImport, setStudentImport] = useState<StudentData[]>([]);

  const { data: response } = useListShifting();
  const { importShift, status } = useImportShift();

  const handleDownloadData = (): void => {
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    const data: any[] = response?.data?.docs || [];
    const newData: any[] = [];

    data.forEach((obj: any) => {
      const newObj: { [key: string]: any } = { name: obj.fullName };
      obj.shiftings.forEach((shifting: any) => {
        const shiftingDate: string = shifting.shiftingDate;
        const inTime: string = shifting.inTime;
        const outTime: string = shifting.outTime;
        newObj[shiftingDate] = `${inTime}-${outTime}`;
      });
      newData.push(newObj);
    });

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newData);
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
    XLSX.writeFile(wb, "data-shifting.xlsx");
  };
  const handleFile = useCallback((file: File) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = e.target?.result as ArrayBuffer;
      const workbook = XLSX.read(data, { type: "binary" });
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      const dataParse = XLSX.utils.sheet_to_json(sheet);
      const finalyData = dataParse?.map((val: any) => {
        return {
          ...val,
          nisn: val?.nisn?.toString(),
          nis: val?.nis?.toString(),
        };
      }) as StudentData[];
      setStudentImport(finalyData);
    };
    reader.readAsBinaryString(file);
  }, []);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    acceptedFiles,
    fileRejections,
  } = useDropzone({
    onDrop: (acceptedFiles) => {
      if (acceptedFiles && acceptedFiles[0]) {
        handleFile(acceptedFiles[0]);
      }
    },
    accept: {
      "text/csv": [
        ".csv, .xlsx, .xls, text/csv, application/vnd.ms-excel, application/csv, text/x-csv, application/x-csv, text/comma-separated-values, text/x-comma-separated-values",
      ],
    },
    maxFiles: 1,
  });

  const handleImport = async () => {
    await importShift(studentImport);
    setStudentImport([]);
  };

  const files = acceptedFiles.map((file) => (
    <p className="text-sm text-center">
      {file.name} - {file.size} bytes
    </p>
  ));

  return (
    // <General>
    <div className="w-full flex  items-center justify-st flex-col">
      <div className=" w-8/12">
        <BaseButton className="my-4 scale-90" onClick={handleDownloadData}>
          <DownloadCloud className="w-5 h-5 mr-3" />
          Download Data Shifting
        </BaseButton>
      </div>
      <div className="bg-white rounded-lg text-[#22272EAB] flex py-5 justify-center items-center flex-col w-8/12">
        <div
          {...getRootProps()}
          className={`hover:cursor-pointer w-10/12 text-gray-700   ${
            isDragActive && "bg-neutral-200"
          } flex flex-col justify-center items-center hover:bg-neutral-200 rounded-xl py-5`}
        >
          <input {...getInputProps()} />

          {/* <Image src={dragIcon} alt="cooo" /> */}
          <UploadCloud className="h-24 w-24 shrink-0 opacity-30 mb-5" />

          <p className="text-sm text-center px-8">
            {isDragActive
              ? "Lepaskan file disini..."
              : "Silahkan pilih file excel yang berisi data shifting pegawai Drag drop file excel disini, atau klik untuk memilih file"}
          </p>
          {files && fileRejections.length > 0 ? (
            <p className="text-red-500 mt-2">
              Hanya file Excel (CSV, XLS, XLSX) yang diperbolehkan!
            </p>
          ) : (
            studentImport.length != 0 && files
          )}
        </div>
        <BaseButton
          className="px-14 mt-5"
          onClick={handleImport}
          isLoading={status === "fetching"}
          disabled={acceptedFiles.length === 0}
        >
          Upload
        </BaseButton>
      </div>
    </div>
    // </General>
  );
};

export default page;
