import { useGetQuery, useAsync } from "@/lib/client/hooks";
import * as services from "../services";
import * as TYPES from "../constants";
import { useEffect } from "react";
import { useToast } from "@/components/shacdn/ui";

export function useListShifting() {
  const { data, status, errorMessage, refetch } = useGetQuery({
    queryKey: TYPES.FETCH_LIST,
    queryFn: async () => {
      const response = await services.list();
      return response.data;
    },
  });

  return {
    data,
    status,
    errorMessage,
    refetch,
  };
}

export function useImportShift() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.importShift
  );

  // const { setResult } = useEmployee();
  const { toast } = useToast();
  useEffect(() => {
    if (status === "resolved") {
      toast({
        title: "Success import shifting",
        // description: "Please wait...",
      });
      // setResult(value);
    }
    if (status === "rejected") {
      toast({
        variant: "destructive",
        title: "Error import shifting",
        description: errorMessage,
      });
    }
  }, [status]);

  async function importShift(payload: any) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    importShift,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}
