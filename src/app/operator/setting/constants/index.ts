const NAMESPACE = "SETTINGS";

export const ENDPOINT = {
  list: "/web-apps/company/profile",
};

export const FETCH_SETTING = `${NAMESPACE}_FETCH_SETTING`;
