"use client";
// import General from "@/layouts/general";
import React from "react";
import { MiniStatic } from "../dashboard/components";
import { BaseButton, TextField } from "@/components/base-components";
import { useListSetting, useUpdateSetting } from "./context/hook";
import { useFormik } from "formik";
import TimesField from "@/components/base-components/time-field";

const page = () => {
  const { data, refetch } = useListSetting();
  const { update, status } = useUpdateSetting();

  const formik = useFormik({
    initialValues: {
      inTime: "",
      outTime: "",
    },
    onSubmit: async (val) => {
      await update(val);
      refetch();
      formik.resetForm();
    },
  });
  console.log(formik.values);

  return (
    <div className=" justify-center w-12/12 md:w-6/12 lg:w-6/12  xl:w-6/12 mx-auto">
      <div className="grid grid-cols-2 gap-10 w-full">
        <MiniStatic
          tittle="Jam Masuk"
          className="bg-white"
          value={data?.data?.inTime || "00:00"}
        />
        <MiniStatic
          tittle="Jam Masuk"
          className="bg-white"
          value={data?.data?.outTime || "00:00"}
        />
      </div>
      <div className="bg-white rounded-xl px-5 py-4 mt-5">
        <h3 className="text-2xl font-semibold text-center">
          Setting Jam Kerja
        </h3>
        <form className="mt-5" onSubmit={formik.handleSubmit}>
          <div className="grid grid-cols-2 gap-5">
            <TimesField
              label="Jam Masuk"
              onChange={(e: any) =>
                formik.setFieldValue("inTime", e.target.value)
              }
              value={formik.values.inTime}
            />

            <TimesField
              label="Jam Masuk"
              onChange={(e: any) =>
                formik.setFieldValue("outTime", e.target.value)
              }
              value={formik.values.outTime}
            />
          </div>
          <div className="flex justify-center">
            <BaseButton
              className="mt-5  px-20"
              isLoading={status === "fetching"}
              type="submit"
            >
              Submit
            </BaseButton>
          </div>
        </form>
      </div>
    </div>
  );
};

export default page;
