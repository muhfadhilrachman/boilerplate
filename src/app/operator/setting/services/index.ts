import client from "../../../../lib/client";
// import {
//   QueryParamsEmplooye,
//   CreateEmployeeType,
//   EmplooyeType,
// } from "../types";
import { ENDPOINT } from "../constants";

function listSetting() {
  return client.get(ENDPOINT.list, {
    // params: {
    //   ...args,
    // },
  });
}

function updateSetting(args: any) {
  return client.put(ENDPOINT.list, {
    params: {
      ...args,
    },
  });
}

export {
  listSetting,
  updateSetting,
  //   updateEmployee,
};
