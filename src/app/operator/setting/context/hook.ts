import { ResponGet } from "@/lib/types";
import { useGetQuery, useAsync } from "@/lib/client/hooks";
import * as TYPES from "../constants";
import * as services from "../services";

export function useListSetting() {
  const { data, status, errorMessage, refetch, isFetching } = useGetQuery<any>({
    queryKey: TYPES.FETCH_SETTING,
    queryFn: async () => {
      const response = await services.listSetting();
      return response.data;
    },
  });
  // const datas = s
  return {
    data,
    status,
    errorMessage,
    isFetching,
    refetch,
  };
}

export function useUpdateSetting() {
  const { execute, status, value, errorMessage, fieldErrors } = useAsync(
    services.updateSetting
  );
  async function update(payload: any) {
    try {
      await execute(payload);
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  return {
    update,
    status,
    value,
    errorMessage,
    fieldErrors,
  };
}
