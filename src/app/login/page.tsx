"use client";
import React, { useEffect } from "react";
import bg from "../../assets/bg-login.svg";
import logo from "../../assets/logo.svg";
import Image from "next/image";
import {
  BaseButton,
  LoadingPage,
  TextField,
} from "@/components/base-components";
import { useFormik } from "formik";
import { Login } from "./types";
import * as Yup from "yup";
import { useLogin } from "@/lib/auth-provider/context/hook";
import { useToast } from "@/components/shacdn/ui";
import { useRouter } from "next/navigation";
import { useAuth } from "@/lib/auth-provider/context";

const Login = () => {
  const router = useRouter();
  const { toast } = useToast();
  const { login, status, errorMessage, value } = useLogin();
  const { user, getAuth } = useAuth();

  const initialValues: Login = {
    email: "",
    password: "",
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().required(),
    password: Yup.string().required(),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (val) => {
      await login(val);
    },
  });

  useEffect(() => {
    status === "resolved" &&
      toast({
        title: "Login Success",
        description: "Please wait...",
      });
    status === "rejected" &&
      toast({
        variant: "destructive",
        title: "Login Error",
        description: errorMessage,
      });
  }, [status]);

  useEffect(() => {
    const currentUser = getAuth();

    if (currentUser.token && currentUser.user.role === "admin") {
      router.push("/operator/dashboard");
    }
    if (currentUser.token && currentUser.user.role === "user") {
      router.push("/user/dashboard");
    }
  }, [user]);

  return (
    <div className="h-screen grid grid-cols-2 text-[#323544DE]">
      <div className=" flex justify-center items-center flex-col">
        <Image src={logo} alt="logo" />
        <span className="underline-offset-1">
          Helo, please login and continue
        </span>

        <form action="" className="mt-4 w-7/12" onSubmit={formik.handleSubmit}>
          <TextField
            label="Email address"
            placeholder="user@gmail.com"
            className="mb-2"
            type="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            errorMessage={formik.errors.email}
            isInvalid={!!formik.errors.email && !!formik.touched.email}
          />

          <TextField
            label="password"
            placeholder="password123"
            className="mb-2"
            type="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            isInvalid={!!formik.errors.password && !!formik.touched.password}
            errorMessage={formik.errors.password}
          />
          <BaseButton
            className="w-full mt-5"
            type="submit"
            isLoading={status === "fetching"}
          >
            Sign In
          </BaseButton>
        </form>
      </div>
      <div className="relative h-screen">
        <Image
          src={bg}
          alt="bg"
          layout="fill"
          objectFit="cover"
          objectPosition="center"
        />
      </div>
    </div>
  );
};

export default Login;
