import { Ban } from "lucide-react";
import React from "react";
import { BaseButton } from ".";

interface Props {
  refetch: any;
}

const DataError = ({ refetch }: Props) => {
  return (
    <div className=" w-full my-5 flex justify-center items-center flex-col">
      <Ban className="shrink-0 opacity-30 h-20 w-20" />
      <p className="text-gray-400">Something Error!</p>
      <BaseButton size={"sm"} className="mt-2 py-1" onClick={refetch}>
        Refresh
      </BaseButton>
    </div>
  );
};

export default DataError;
