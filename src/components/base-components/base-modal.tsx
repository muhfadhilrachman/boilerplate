import React from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/shacdn/ui/alert-dialog";
import { Button } from "./base-button";
import { Cross2Icon } from "@radix-ui/react-icons";

interface Props {
  isOpen: boolean;
  onClose: any;
  tittle?: string;
  children: React.ReactNode;
  onSubmit?: React.MouseEventHandler<HTMLButtonElement>;
}

const BaseModal = ({ isOpen, onClose, tittle, children, onSubmit }: Props) => {
  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      {/* <AlertDialogTrigger></AlertDialogTrigger> */}
      <AlertDialogContent>
        <AlertDialogHeader className="relative ">
          <AlertDialogTitle className=" text-center">{tittle}</AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
          <AlertDialogDescription>{children}</AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter className="flex justify-center items-center">
          <Button type="submit" className="px-20" onClick={onSubmit}>
            Submit
          </Button>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default BaseModal;
