import React from "react";
import { CommandInput } from "../shacdn/ui/command";
import { BaseInput } from ".";
import { Search } from "lucide-react";

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  isInvalid?: boolean;
}

const InputSearch = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, ...props }, ref) => {
    return (
      <div className="relative">
        <BaseInput className={`pr-10 ${className}`} ref={ref} {...props} />
        <Search className="textxs shrink-0 opacity-30 absolute right-3 h-5 w-5 top-2" />
      </div>
    );
  }
);
export default InputSearch;
