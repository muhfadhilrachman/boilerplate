import { LayoutGrid } from "lucide-react";
import { Popover, PopoverContent, PopoverTrigger } from "../shacdn/ui/popover";

import React from "react";

interface Props {
  children?: React.ReactNode;
  className?: string;
}
const ButtonAction = ({ children, className }: Props) => {
  return (
    <Popover>
      <PopoverTrigger asChild>
        {/* <Button variant="outline">Open popover</Button> */}
        <div className="bg-[#E26E8530] rounded-xl p-2 w-max hover:cursor-pointer">
          <LayoutGrid className="w-4 h-4 text-[#CB3560]" />
        </div>
      </PopoverTrigger>
      <PopoverContent
        className={`w-max py-3 px-4 ${className}`}
        side="bottom"
        align="end"
      >
        {children}
      </PopoverContent>
    </Popover>
  );
};

export default ButtonAction;
