import React from "react";
import { BaseInput } from ".";

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  isInvalid?: boolean;
  errorMessage?: string;
  label: string;
  labelClassname?: string;
}

const TextField = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      className,
      type,
      labelClassname,
      isInvalid = false,
      errorMessage,
      label,
      disabled,
      ...props
    },
    ref
  ) => {
    return (
      <div className={className}>
        <p
          className={
            labelClassname + " text-left text-xs text-rose font-semibold"
          }
        >
          {label}
        </p>
        <BaseInput
          className={`mt-1 ${disabled && "bg-slate-200"}`}
          disabled={disabled}
          isInvalid={isInvalid}
          ref={ref}
          type={type}
          {...props}
        />
        {isInvalid && <small className="text-red-500">{errorMessage}</small>}
      </div>
    );
  }
);

export default TextField;
