import React from "react";
import { BaseInput } from ".";
import TimeField from "react-simple-timefield";

export interface InputProps {
  isInvalid?: boolean;
  errorMessage?: string;
  label: string;
  labelClassname?: string;
  value?: string;
  onChange: any;
  className?: string;
}

const TimesField = ({
  className,
  labelClassname,
  isInvalid = false,
  errorMessage,
  label,
  value,
  onChange,
}: InputProps) => {
  return (
    <div className={className}>
      <p
        className={
          labelClassname + " text-left text-xs text-rose font-semibold"
        }
      >
        {label}
      </p>
      <TimeField
        onChange={onChange}
        input={<BaseInput isInvalid={isInvalid} />}
        value={value}
      />

      {isInvalid && <small className="text-red-500">{errorMessage}</small>}
    </div>
  );
};

export default TimesField;
