"use client";
import Image from "next/image";
import React from "react";
import logo from "../../assets/logo.svg";
const LoadingPage = () => {
  return (
    <div className="bg-white bg-opacity-90 h-screen z-50 absolute w-screen flex justify-center items-center">
      <div>
        <Image src={logo} alt="logo" />
        {/* <span>Please wait...</span> */}
      </div>
    </div>
  );
};

export default LoadingPage;
