import {
  DoubleArrowLeftIcon,
  DoubleArrowRightIcon,
} from "@radix-ui/react-icons";

interface PaginationProps {
  totalItems: number;
  currentPage: number;
  pageCount: number;
  gotoPage: (page: number) => void;
  previousPage: () => void;
  nextPage: () => void;
  maxVisible?: number;
  limit?: number;
}

const Pagination: React.FC<PaginationProps> = ({
  totalItems,
  currentPage,
  pageCount,
  gotoPage,
  previousPage,
  nextPage,
  maxVisible = 5,
  limit,
}) => {
  const visiblePages = Math.min(maxVisible, pageCount);
  const visiblePagesStart = Math.max(
    1,
    currentPage - Math.floor(visiblePages / 2)
  );
  const visiblePagesEnd = Math.min(
    pageCount,
    visiblePagesStart + visiblePages - 1
  );
  const visiblePagesArray = Array.from(
    { length: visiblePagesEnd - visiblePagesStart + 1 },
    (v, k) => k + visiblePagesStart
  );
  const showing = `Showing ${
    limit
      ? totalItems < limit
        ? totalItems
        : limit
      : totalItems < 10
      ? totalItems
      : 10
  } of ${totalItems} entries`;

  return (
    <div className="flex justify-between items-center px-4 mt-4">
      <span className="text-gray-700 text-sm">{showing}</span>
      <div className="w-max flex justify-between items-center">
        {currentPage != 1 && (
          <DoubleArrowLeftIcon
            className="text-rose mr-3 hover:text-pink-700 hover:cursor-pointer"
            onClick={() => previousPage()}
          />
        )}
        {visiblePagesArray.map((val) => {
          return (
            <span
              className={`text-rose mr-3 hover:cursor-pointer  ${
                val === currentPage ? "font-bold" : "hover:text-pink-700"
              }`}
              onClick={() => gotoPage(val)}
            >
              {val}
            </span>
          );
        })}
        {currentPage < pageCount && (
          <DoubleArrowRightIcon
            className="text-rose ml-3 hover:text-pink-700 hover:cursor-pointer"
            onClick={() => nextPage()}
          />
        )}
      </div>
    </div>
  );
};

export default Pagination;
