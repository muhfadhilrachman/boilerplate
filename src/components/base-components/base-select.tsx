import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/shacdn/ui/select";
import React from "react";
import { ScrollArea } from "../shacdn/ui/scroll-area";

interface Options {
  value: string | number;
  label: string | number;
}

interface Props {
  options: Options[];
  className?: string;
  name?: string;
  id?: string;
  value?: string | number;
  isInvalid?: boolean;
  onChange?: any;
  placeholder: React.ReactNode;
  variant?: "rose";
}
const BaseSelect = ({
  className,
  options,
  name,
  id,
  value,
  onChange,
  variant,
  placeholder,
  isInvalid,
}: Props) => {
  return (
    <Select onValueChange={onChange} name={name} value={value?.toString()}>
      <SelectTrigger
        variant={variant}
        className={className + " max-w-[500px]"}
        isInvalid={isInvalid}
      >
        {value ? <SelectValue /> : <>{placeholder}</>}
      </SelectTrigger>
      <SelectContent>
        <ScrollArea className="overflow-y-scroll max-h-48">
          <SelectItem value="">All</SelectItem>
          {options.map((val, key) => {
            return (
              <SelectItem key={key} value={val.value.toString()}>
                {val.label}
              </SelectItem>
            );
          })}
        </ScrollArea>
      </SelectContent>
    </Select>
  );
};

export default BaseSelect;
