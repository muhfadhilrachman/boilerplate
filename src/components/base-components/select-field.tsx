import { BaseSelect } from ".";

import React from "react";

interface Options {
  value: string | number;
  label: string | number;
}

interface Props {
  className?: string;
  label?: string;
  isInvalid?: boolean;
  errorMessage?: string;
  options: Options[];
  onChange?: any;
  value?: string;
  placeholder: string;
}

const SelectField = ({
  className,
  label,
  isInvalid,
  errorMessage,
  options,
  onChange,
  value,
  placeholder,
}: Props) => {
  return (
    <div className={className}>
      <p className="mb-3 text-left text-xs text-rose font-semibold">{label}</p>
      <BaseSelect
        className="mt-2"
        options={options}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
        isInvalid={isInvalid}
      />
      {isInvalid && <small className="text-red-500">{errorMessage}</small>}
    </div>
  );
};

export default SelectField;
