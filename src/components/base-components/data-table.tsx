"use client";

import { useTable } from "react-table";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/shacdn/ui/table";
import TableSkeleton from "./table-skeleton";
import { SearchX } from "lucide-react";

interface PropsTable<T extends object> {
  isLoaded?: boolean;
  data: Array<T>;
  customColumn?: any;
  columns: any;
  currentPage: number;
  limit: number;
}

export default function DataTable<T extends object>({
  isLoaded = false,
  data,
  customColumn,
  currentPage,
  limit,
  columns,
  ...props
}: PropsTable<T>) {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({ columns, data });

  return (
    <div {...props}>
      {!isLoaded && <TableSkeleton />}
      {isLoaded && data.length != 0 && (
        <Table {...getTableProps()}>
          <TableHeader>
            {headerGroups.map((headerGroups) => (
              <TableRow
                {...headerGroups.getHeaderGroupProps()}
                className="border-b-rose"
              >
                <TableHead>No</TableHead>
                {headerGroups.headers.map((columns) => (
                  <TableHead {...columns.getHeaderProps()}>
                    {columns.render("Header")}
                  </TableHead>
                ))}
              </TableRow>
            ))}
          </TableHeader>

          <TableBody {...getTableBodyProps()}>
            {rows.map((rows, i) => {
              prepareRow(rows);

              return (
                <TableRow
                  {...rows.getRowProps()}
                  {...customColumn}
                  // onClick={() => customColumn?.onClick(rows) || null}
                >
                  <TableCell>{++i + (currentPage * limit - limit)}</TableCell>
                  {rows.cells.map((cell: any) => {
                    return (
                      <TableCell {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )}
      {isLoaded && data.length == 0 && (
        <div className="w-full flex flex-col justify-center items-center text-gray-400 mt-12 ">
          <SearchX className="shrink-0 opacity-30 h-20 w-20" />
          <span className="mt-2">No Results Found</span>
        </div>
      )}
    </div>
  );
}
