import React from "react";
import {
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialog,
} from "../shacdn/ui";
import { Cross2Icon } from "@radix-ui/react-icons";
import { BaseButton } from ".";

interface Props {
  isOpen: boolean;
  onClose: any;
  tittle?: string;
  onSubmit?: React.MouseEventHandler<HTMLButtonElement>;
  // listEmployee: Array<{ value: string; label: string }>;
}
const ModalDelete = ({ isOpen, onClose, onSubmit, tittle }: Props) => {
  return (
    <AlertDialog open={isOpen} onOpenChange={onClose}>
      <AlertDialogContent>
        <AlertDialogHeader className="relative ">
          <AlertDialogTitle>Delete {tittle}</AlertDialogTitle>
          <div
            className="border-2 absolute -top-4 -right-2 border-rose h-4 w-4 hover:cursor-pointer flex justify-center items-center rounded-md"
            onClick={onClose}
          >
            <Cross2Icon className="text-[5px] text-rose font-semibold" />
          </div>
        </AlertDialogHeader>

        <AlertDialogDescription>
          <p>Are you sure you want to delete this data?</p>
        </AlertDialogDescription>
        <AlertDialogFooter className="flex justify-end">
          <BaseButton onClick={onSubmit}>Delete</BaseButton>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default ModalDelete;
