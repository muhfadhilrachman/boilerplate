"use client";
import React from "react";
import { Menu, User } from "lucide-react";
const NavbarUser = () => {
  return (
    <div className="flex justify-between items-center  py-3 ">
      <h3 className="text-[#4E5560] text-[16px]">Hi, User</h3>
      <div className="w-10 h-10 bg-[#E06B804D] rounded-full flex items-center justify-center">
        <User className="text-rose h-6 w-6" />
      </div>
    </div>
  );
};

export default NavbarUser;
