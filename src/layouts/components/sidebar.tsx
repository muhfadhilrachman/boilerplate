"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import logo from "../../assets/logo.svg";
import { usePathname } from "next/navigation";
import Link from "next/link";
import useMenu from "../context";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
} from "@/components/shacdn/ui/sheet";
import { LIST_SIDEBAR } from "../constant";
const Sidebar = () => {
  // const { data } = useListSetting();
  const pathName = usePathname();
  const { menu, setMenu } = useMenu();
  const [windowSize, setWindowSize] = useState({
    width: typeof window !== "undefined" ? window.innerWidth : 0,
    height: typeof window !== "undefined" ? window.innerHeight : 0,
  });

  useEffect(() => {
    const handleResize = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []); // [] menandakan bahwa effect hanya berjalan sekali setelah komponen pertama kali dimuat

  return (
    <>
      <div
        className={`pl-4  py-12 hidden md:block    transition-all duration-300 ${
          menu ? "w-[270px]" : "w-[0px] overflow-x-hidden "
        }`}
      >
        <Image src={logo} alt="logo" />
        <p className="font-semibold text-slate-700 text-sm ">Hello,</p>
        <ul className="mt-10 text-sm">
          {LIST_SIDEBAR.map((val, key) => {
            return (
              <Link href={val.href}>
                <li
                  key={key}
                  className={`px-6 py-2.5 text-black   flex mt-2  items-center hover:cursor-pointer transition-all duration-300  ${
                    val.href === pathName &&
                    "bg-rose text-white rounded-xl font-semibold"
                  }`}
                >
                  {val.icon}
                  <span className="ml-4">{val.name}</span>
                </li>
              </Link>
            );
          })}
        </ul>
      </div>
      <Sheet
        open={windowSize.width <= 768 && menu}
        onOpenChange={() => setMenu(!menu)}
      >
        {/* <SheetTrigger>Open</SheetTrigger> */}
        <SheetContent side="left" className="block md:hidden w-[300px]">
          <SheetHeader>
            {/* <SheetTitle>Are you sure absolutely sure?</SheetTitle> */}
            <SheetDescription>
              <div className={`  py-12  transition-all duration-300 `}>
                <Image src={logo} alt="logo" />
                <p className="font-semibold text-slate-700 text-sm ">
                  Hello, Super Admin
                </p>
                <ul className="mt-10 text-sm">
                  {LIST_SIDEBAR.map((val, key) => {
                    return (
                      <Link href={val.href} onClick={() => setMenu(!menu)}>
                        <li
                          key={key}
                          className={`px-6 py-2.5 flex mt-2  items-center hover:cursor-pointer transition-all duration-300  ${
                            val.href === pathName &&
                            "bg-rose text-white rounded-xl font-semibold"
                          }`}
                        >
                          {val.icon}
                          <span className="ml-4">{val.name}</span>
                        </li>
                      </Link>
                    );
                  })}
                </ul>
              </div>
            </SheetDescription>
          </SheetHeader>
        </SheetContent>
      </Sheet>
    </>
  );
};

export default Sidebar;
