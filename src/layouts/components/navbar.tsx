"use client";
import React from "react";
import { PersonIcon, RowsIcon } from "@radix-ui/react-icons";
import { usePathname } from "next/navigation";
import useMenu from "../context";
import { Menu } from "lucide-react";
const Navbar = () => {
  const pathname = usePathname().split("/")[2].toUpperCase();

  const { menu, setMenu } = useMenu();

  return (
    <div className="flex justify-between items-center ">
      <div className="flex items-center ">
        <div
          className="mr-2 h-10 w-10  rounded-full bg-pink-200 flex items-center justify-center hover:cursor-pointer hover:opacity-90"
          onClick={() => setMenu(!menu)}
        >
          {/* <RowsIcon /> */}
          <Menu className="text-[#CB3560] h-5 w-5" />
        </div>
        <p className="text-2xl ">{pathname}</p>
      </div>
      <div className="w-12 h-12 bg-rose rounded-full flex items-center justify-center">
        <PersonIcon className="text-white" />
      </div>
    </div>
  );
};

export default Navbar;
