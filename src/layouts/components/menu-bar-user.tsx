"use client";
import React from "react";
import { Camera, ClipboardList, LayoutDashboard, User } from "lucide-react";
import Link from "next/link";
import { usePathname } from "next/navigation";

const MenuBarUser = () => {
  const pathName = usePathname();

  //   const color = {`mr-4`}
  const listSidebar = [
    {
      link: "/user/dashboard",
      icon: (
        <LayoutDashboard
          className={`mr-4 text-[#63737A]  ${
            pathName === "/user/dashboard" && "text-rose"
          }`}
        />
      ),
    },
    {
      link: "/user/absent",
      icon: (
        <Camera
          className={`mr-4 text-[#63737A]  ${
            pathName === "/user/absent" && "text-rose"
          }`}
        />
      ),
    },

    {
      link: "/user/history",
      icon: (
        <ClipboardList
          className={`mr-4 text-[#63737A]  ${
            pathName === "/user/history" && "text-rose"
          }`}
        />
      ),
    },
    {
      link: "/user/profile",
      icon: (
        <User
          className={`mr-4 text-[#63737A]  ${
            pathName === "/user/profile" && "text-rose"
          }`}
        />
      ),
    },
  ];
  return (
    <div className="bg-white flex shadow-lg   bottom-2 rounded-xl p-3 mt-5 justify-around w-full">
      {listSidebar.map((val, key) => {
        return (
          <Link href={val.link} key={key}>
            {val.icon}
          </Link>
        );
      })}
    </div>
  );
};

export default MenuBarUser;
