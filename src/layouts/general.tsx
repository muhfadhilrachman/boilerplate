"use client";
import React, { ReactNode } from "react";
import SecretPage from "./secret-page";
import dynamic from "next/dynamic";

const Sidebar = dynamic(() => import("./components/sidebar"));
// const SecretPage = dynamic(()=>import('./SecretPage'))
const Navbar = dynamic(() => import("./components/navbar"));

interface Props {
  children?: ReactNode;
  className?: string;
}

const General = ({ children, className }: Props) => {
  return (
    <SecretPage>
      <div className="flex">
        <Sidebar />
        <div className="min-h-screen bg-[#F7F8FA] w-full rounded-3xl my-4 px-7 py-5 mr-7 ml-7">
          <Navbar />
          <div className={`mt-14 ${className}`}>{children}</div>
        </div>
      </div>
    </SecretPage>
  );
};

export default General;
