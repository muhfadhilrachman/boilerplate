import { useAuth } from "@/lib/auth-provider/context";
// import { useRouter } from "next/navigation";
import React, { Suspense, useEffect } from "react";
import { redirect } from "next/navigation";
const SecretPage = ({ children }: { children: React.ReactNode }) => {
  const { getAuth, token } = useAuth();

  useEffect(() => {
    const auth = getAuth();
    if (!auth.isAuth) {
      // router.push("/login");
      redirect("/login");
    }
  }, [token]);

  return <>{children}</>;
};

export default SecretPage;
