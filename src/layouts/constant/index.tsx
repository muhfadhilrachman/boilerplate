import {
  Monitor,
  Users,
  Building,
  Clock,
  ClipboardList,
  Megaphone,
  Settings,
} from "lucide-react";

const LIST_SIDEBAR = [
  {
    name: "Dashboard",
    href: "/operator/dashboard",
    icon: <Monitor className="w-4 h-4" />,
  },
  {
    name: "Pegawai",
    href: "/operator/employee",
    icon: <Users className="w-4 h-4" />,
  },
  {
    name: "Directorate",
    href: "/operator/directorate",
    icon: <Building className="w-4 h-4" />,
  },
  {
    name: "Attendance",
    href: "/operator/attendance",
    icon: <Clock className="w-4 h-4" />,
  },
  {
    name: "Shifting",
    href: "/operator/shifting",
    icon: <ClipboardList className="w-4 h-4" />,
  },
  {
    name: "Report",
    href: "/operator/report",
    icon: <Megaphone className="w-4 h-4" />,
  },
  {
    name: "Setting",
    href: "/operator/setting",
    icon: <Settings className="w-4 h-4" />,
  },
];

export { LIST_SIDEBAR };
