import create from "zustand";

interface TypeState {
  menu: boolean;
  setMenu: (state: boolean) => void;
}

const useMenu = create<TypeState>((set) => ({
  menu: true,
  setMenu: (state) => set({ menu: state }),
}));

export default useMenu;
