"use client";

import React, { ReactNode } from "react";
import dynamic from "next/dynamic";
const MenuBarUser = dynamic(() => import("./components/menu-bar-user"));
const NavbarUser = dynamic(() => import("./components/navbar-user"));

interface Props {
  children?: ReactNode;
  className?: string;
}

const UserLayout = ({ children, className }: Props) => {
  return (
    <div className="max-w-[420px]  min-h-[600px] text-[#4E5560] pb-5 bg-[#F7F8FA] rounded-xl shadow-md my-3 mx-auto border px-5 ">
      <NavbarUser />
      <div className=" mt-2">
        <p className="text-sm mb-1">23 Agustus 2023</p>
        {/* <h3 className="text-3xl font-extrabold">22:30</h3> */}
      </div>
      {children}
      {/* <div className="fixed bg-red-200 flex items-center justify-center border  w-full left-0 right-0 bottom-10"> */}
      <MenuBarUser />
    </div>
  );
};

export default UserLayout;
